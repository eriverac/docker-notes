## 1. Introducción
***
___$ docker --version___
~~~
Client:
 Version:           18.09.1
 API version:       1.39
 Go version:        go1.11.6
 Git commit:        4c52b90
 Built:             Tue, 03 Sep 2019 19:59:35 +0200
 OS/Arch:           linux/amd64
 Experimental:      false

Server:
 Engine:
  Version:          18.09.1
  API version:      1.39 (minimum version 1.12)
  Go version:       go1.11.6
  Git commit:       4c52b90
  Built:            Tue Sep  3 17:59:35 2019
  OS/Arch:          linux/amd64
  Experimental:     false
~~~
___$ sudo usermod -aG docker $USER___

___$ id___

~~~
uid=1000(gvillafu) gid=1000(gvillafu) groups=1000(gvillafu),24(cdrom),25(floppy),29(audio),30(dip),44(video),46(plugdev),101(systemd-journal),109(netdev),127(libvirt),128(bluetooth)
~~~
___$ docker___

~~~
Usage:	docker [OPTIONS] COMMAND

A self-sufficient runtime for containers

Options:
      --config string      Location of client config files (default "/root/.docker")
  -D, --debug              Enable debug mode
  -H, --host list          Daemon socket(s) to connect to
  -l, --log-level string   Set the logging level ("debug"|"info"|"warn"|"error"|"fatal") (default "info")
      --tls                Use TLS; implied by --tlsverify
      --tlscacert string   Trust certs signed only by this CA (default "/root/.docker/ca.pem")
      --tlscert string     Path to TLS certificate file (default "/root/.docker/cert.pem")
      --tlskey string      Path to TLS key file (default "/root/.docker/key.pem")
      --tlsverify          Use TLS and verify the remote
  -v, --version            Print version information and quit

Management Commands:
  builder     Manage builds
  config      Manage Docker configs
  container   Manage containers
  engine      Manage the docker engine
  image       Manage images
  network     Manage networks
  node        Manage Swarm nodes
  plugin      Manage plugins
  secret      Manage Docker secrets
  service     Manage services
  stack       Manage Docker stacks
  swarm       Manage Swarm
  system      Manage Docker
  trust       Manage trust on Docker images
  volume      Manage volumes

Commands:
  attach      Attach local standard input, output, and error streams to a running container
  build       Build an image from a Dockerfile
  commit      Create a new image from a container's changes
  cp          Copy files/folders between a container and the local filesystem
  create      Create a new container
  diff        Inspect changes to files or directories on a container's filesystem
  events      Get real time events from the server
  exec        Run a command in a running container
  export      Export a container's filesystem as a tar archive
  history     Show the history of an image
  images      List images
  import      Import the contents from a tarball to create a filesystem image
  info        Display system-wide information
  inspect     Return low-level information on Docker objects
  kill        Kill one or more running containers
  load        Load an image from a tar archive or STDIN
  login       Log in to a Docker registry
  logout      Log out from a Docker registry
  logs        Fetch the logs of a container
  pause       Pause all processes within one or more containers
  port        List port mappings or a specific mapping for the container
  ps          List containers
  pull        Pull an image or a repository from a registry
  push        Push an image or a repository to a registry
  rename      Rename a container
  restart     Restart one or more containers
  rm          Remove one or more containers
  rmi         Remove one or more images
  run         Run a command in a new container
  save        Save one or more images to a tar archive (streamed to STDOUT by default)
  search      Search the Docker Hub for images
  start       Start one or more stopped containers
  stats       Display a live stream of container(s) resource usage statistics
  stop        Stop one or more running containers
  tag         Create a tag TARGET_IMAGE that refers to SOURCE_IMAGE
  top         Display the running processes of a container
  unpause     Unpause all processes within one or more containers
  update      Update configuration of one or more containers
  version     Show the Docker version information
  wait        Block until one or more containers stop, then print their exit codes

Run 'docker COMMAND --help' for more information on a command.
~~~
___$ docker images___

~~~
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
~~~

___$ docker image pull fernando93d/hello___
~~~
Using default tag: latest
latest: Pulling from fernando93d/hello
a5a6f2f73cd8: Pull complete
3a6fba040982: Pull complete
973ed4320c0c: Pull complete
2e4c0b09f607: Pull complete
c5f847ace1b0: Pull complete
744795507d35: Pull complete
6c056974fa65: Pull complete
5baf43676d94: Pull complete
01c71a34f7dd: Pull complete
Digest: sha256:34f01b0fbfc1945ee3ae700d3474cae2dafa91e6bedb8965ecb653bc160297c3
Status: Downloaded newer image for fernando93d/hello:latest
~~~
___$ docker image ls___
~~~
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
fernando93d/hello   latest              7fe934464560        16 months ago       150MB
~~~
___$ docker container___
~~~
Usage:	docker container COMMAND

Manage containers

Commands:
  attach      Attach local standard input, output, and error streams to a running container
  commit      Create a new image from a container's changes
  cp          Copy files/folders between a container and the local filesystem
  create      Create a new container
  diff        Inspect changes to files or directories on a container's filesystem
  exec        Run a command in a running container
  export      Export a container's filesystem as a tar archive
  inspect     Display detailed information on one or more containers
  kill        Kill one or more running containers
  logs        Fetch the logs of a container
  ls          List containers
  pause       Pause all processes within one or more containers
  port        List port mappings or a specific mapping for the container
  prune       Remove all stopped containers
  rename      Rename a container
  restart     Restart one or more containers
  rm          Remove one or more containers
  run         Run a command in a new container
  start       Start one or more stopped containers
  stats       Display a live stream of container(s) resource usage statistics
  stop        Stop one or more running containers
  top         Display the running processes of a container
  unpause     Unpause all processes within one or more containers
  update      Update configuration of one or more containers
  wait        Block until one or more containers stop, then print their exit codes

Run 'docker container COMMAND --help' for more information on a command.
~~~
___$ docker container run --help___

___$ docker container run fernando93d/hello___
~~~
    __  __      __         __  ___                __
   / / / /___  / /___ _   /  |/  /_  ______  ____/ /___
  / /_/ / __ \/ / __ `/  / /|_/ / / / / __ \/ __  / __ \
 / __  / /_/ / / /_/ /  / /  / / /_/ / / / / /_/ / /_/ /
/_/ /_/\____/_/\__,_/  /_/  /_/\__,_/_/ /_/\__,_/\____/

~~~
## 2. Contenedores
***
### Comandos básicos
___$ docker container --help___

___$ docker container ls___
~~~
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
~~~

Muestra los contenedores en ejecución

___$ docker container ls -a___
~~~
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS                      PORTS               NAMES
3b5fbe5b0dfd        fernando93d/hello   "python ./app.py"   53 seconds ago      Exited (0) 52 seconds ago                       zealous_bose
~~~
Muestras todos los contenedores en ejecución o no

___$ docker container rm --help___

___$ docker container rm CONTAINER_ID or NAMES___
~~~
3b5fbe5b0dfd
~~~
___$ docker container create IMAGEN_FUENTE___
~~~
7e653edebf1ed0a1871c8b8c6bf65a27f0856a5ba762d14d959b0d8aab16cc96
~~~
___$ docker container ls___
~~~
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
~~~
___$ docker container ls -a___
~~~
CONTAINER ID        IMAGE               COMMAND             CREATED              STATUS              PORTS               NAMES
7e653edebf1e        fernando93d/hello   "python ./app.py"   About a minute ago   Created                                 trusting_volhard
~~~
___$ docker container start CONTAINER_ID or NAMES___
~~~
7e653edebf1e
~~~
___$ docker container ls -a___
~~~
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS                      PORTS               NAMES
7e653edebf1e        fernando93d/hello   "python ./app.py"   2 minutes ago       Exited (0) 24 seconds ago                       trusting_volhard
~~~
___$ docker container stop CONTAINER_ID or NAMES___
~~~
7e653edebf1e
~~~
### Modo interactivo en Docker
___$ docker image pull ubuntu___
~~~
Using default tag: latest
latest: Pulling from library/ubuntu
5bed26d33875: Pull complete
f11b29a9c730: Pull complete
930bda195c84: Pull complete
78bf9a5ad49e: Pull complete
Digest: sha256:bec5a2727be7fff3d308193cfde3491f8fba1a2ba392b7546b43a051853a341d
Status: Downloaded newer image for ubuntu:latest
~~~
Por defecto bajará latest, pero puede definirse un tag

___$ docker image pull ubuntu:19.10___

Bajara la imagen ubuntu con release 19.10

___$ docker imagen ls___
~~~
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
ubuntu              latest              4e5021d210f6        12 days ago         64.2MB
fernando93d/hello   latest              7fe934464560        16 months ago       150MB
~~~
___$ docker container run ubuntu:latest___

Se crea el docker y se detiene o apagará porque no tiene ningun proceso ejecutandose.

___$ docker container ls___
~~~
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
~~~
___$ docker container ls -a___
~~~
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS                      PORTS               NAMES
b1b5ea9d948e        ubuntu:latest       "/bin/bash"         23 seconds ago      Exited (0) 22 seconds ago                       silly_mclaren
7e653edebf1e        fernando93d/hello   "python ./app.py"   7 minutes ago       Exited (0) 5 minutes ago                        trusting_volhard
~~~
___$ docker image pull nginx___
~~~
Using default tag: latest
latest: Pulling from library/nginx
c499e6d256d6: Pull complete
74cda408e262: Pull complete
ffadbd415ab7: Pull complete
Digest: sha256:282530fcb7cd19f3848c7b611043f82ae4be3781cb00105a1d593d7e6286b596
Status: Downloaded newer image for nginx:latest
~~~
___$ docker image ls___
~~~
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
nginx               latest              ed21b7a8aee9        47 hours ago        127MB
ubuntu              latest              4e5021d210f6        12 days ago         64.2MB
fernando93d/hello   latest              7fe934464560        16 months ago       150MB
~~~
___$ docker container run nginx___

La terminal se quedará ejecutando el contenedor en esta terminal.

___$ docker container ls___
~~~
CONTAINER ID        IMAGE               COMMAND                  CREATED              STATUS              PORTS               NAMES
f39c1b9e6c4d        nginx               "nginx -g 'daemon of…"   About a minute ago   Up 58 seconds       80/tcp              eloquent_liskov
~~~

Desde otra terminal visualizar el contenedor de nginx en ejecución.

Ctrl+c para detenerlo en la primer terminal.

___$ docker container run --help___

___$ docker container run -it ubuntu___
~~~
root@11be7be3bba8:/# ls
bin  boot  dev  etc  home  lib  lib64  media  mnt  opt  proc  root  run  sbin  srv  sys  tmp  usr  var
root@11be7be3bba8:/#
~~~
Entrará al contenedor y se podrá ejecutar comandos como "ls".

Desde otra terminal visualizar el contenedor de ubuntu en ejecución.

* -i = interactive

* -t = terminal, esto nos dejará dentro del contenedor, si se da exit, se sale del contenedor y se detiene.

Salirse del contenedor con "exit".

___$ docker container ls___
~~~
CONTAINER ID        IMAGE               COMMAND             CREATED              STATUS              PORTS               NAMES
11be7be3bba8        ubuntu              "/bin/bash"         About a minute ago   Up About a minute                       romantic_morse
~~~
___$ docker container run -itd ubuntu___
~~~
a7eb68fd6895a514e9e290d24abbaabf0ec96fbc998345ff02f942ff415be6fc
~~~
* -d = detach, se ejecutará el contenedor en segundo plano y no se apagará.

___$ docker container ls___
~~~
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
a7eb68fd6895        ubuntu              "/bin/bash"         31 seconds ago      Up 30 seconds                           gallant_hawking
~~~
Se verá el contenedor de ubuntu en ejecución.

___$ docker container run -d nginx___
~~~
8216742a12ec134095f5990651b2cb0f7865201a5efdf6008e1d6ad4eb401c4e
~~~
Se ejecutará en segundo plano y el servicio estará arriba sin caerse, para detenerlo docker container stop CONTAINER_ID o NAMES.

### Ejecutar comandos dentro de un contenedor
___$ docker container ls___
~~~
CONTAINER ID        IMAGE               COMMAND                  CREATED              STATUS              PORTS               NAMES
8216742a12ec        nginx               "nginx -g 'daemon of…"   About a minute ago   Up About a minute   80/tcp              elastic_lovelace
a7eb68fd6895        ubuntu              "/bin/bash"              4 minutes ago        Up 4 minutes                            gallant_hawking
~~~
___$ docker container attach --help___
~~~
Usage:	docker container attach [OPTIONS] CONTAINER

Attach local standard input, output, and error streams to a running container

Options:
      --detach-keys string   Override the key sequence for detaching a container
      --no-stdin             Do not attach STDIN
      --sig-proxy            Proxy all received signals to the process (default true)
~~~
___$ docker container exec --help___
~~~
Usage:	docker container exec [OPTIONS] CONTAINER COMMAND [ARG...]

Run a command in a running container

Options:
  -d, --detach               Detached mode: run command in the background
      --detach-keys string   Override the key sequence for detaching a container
  -e, --env list             Set environment variables
  -i, --interactive          Keep STDIN open even if not attached
      --privileged           Give extended privileges to the command
  -t, --tty                  Allocate a pseudo-TTY
  -u, --user string          Username or UID (format: <name|uid>[:<group|gid>])
  -w, --workdir string       Working directory inside the container
~~~
___$ docker container attach CONTAINER_ID___

Se meterá en el contenedor y se podrá ejecutar comandos, pero si se da exit se sale y detiene el contenedor.

___$ docker container stop $(docker container ls -q)___
~~~
8216742a12ec
a7eb68fd6895
~~~
Detendrá todos los contenedores que esten en ejecución.

___$ docker container ls___
~~~
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
~~~
___$ docker container ls -a___
~~~
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS                      PORTS               NAMES
8216742a12ec        nginx               "nginx -g 'daemon of…"   9 minutes ago       Exited (0) 2 minutes ago                        elastic_lovelace
a7eb68fd6895        ubuntu              "/bin/bash"              12 minutes ago      Exited (0) 2 minutes ago                        gallant_hawking
11be7be3bba8        ubuntu              "/bin/bash"              15 minutes ago      Exited (0) 2 minutes ago                        romantic_morse
f39c1b9e6c4d        nginx               "nginx -g 'daemon of…"   19 minutes ago      Exited (0) 15 minutes ago                       eloquent_liskov
b1b5ea9d948e        ubuntu:latest       "/bin/bash"              22 minutes ago      Exited (0) 22 minutes ago                       silly_mclaren
7e653edebf1e        fernando93d/hello   "python ./app.py"        29 minutes ago      Exited (0) 27 minutes ago                       trusting_volhard
~~~
___$ docker container start a7eb68fd6895___
~~~
a7eb68fd6895
~~~
Levantar nuevamente el contenedor para trabajarlo.

___$ docker container ls___
~~~
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
a7eb68fd6895        ubuntu              "/bin/bash"         15 minutes ago      Up 11 seconds                           gallant_hawking
~~~
___$ docker container exec a7eb68fd6895 ls -al |grep a___
~~~
total 4
drwxr-xr-x   2 root root 4096 Mar 11 21:05 bin
drwxr-xr-x   8 root root   96 May 23  2017 lib
drwxr-xr-x   2 root root   34 Mar 11 21:03 lib64
drwxr-xr-x   2 root root    6 Mar 11 21:03 media
drwxr-xr-x   2 root root    6 Mar 11 21:03 mnt
drwxr-xr-x   2 root root    6 Mar 11 21:03 opt
drwx------   2 root root   37 Mar 11 21:05 root
drwxr-xr-x   1 root root   21 Mar 20 19:20 run
drwxr-xr-x   1 root root   21 Mar 20 19:20 sbin
drwxr-xr-x   2 root root    6 Mar 11 21:03 srv
drwxrwxrwt   2 root root    6 Mar 11 21:05 tmp
drwxr-xr-x   1 root root   18 Mar 11 21:03 usr
drwxr-xr-x   1 root root   17 Mar 11 21:05 var
~~~
___$ docker container exec -it CONTAINER_ID bash___
~~~
root@a7eb68fd6895:/# ls
bin  boot  dev  etc  home  lib  lib64  media  mnt  opt  proc  root  run  sbin  srv  sys  tmp  usr  var
~~~
Entrará al contenedor ejecutando bash y se podrá ejecutar lo que sea, si se da exit el contenedor sigue vivo, se puede ejecutar varias veces para tener varias terminales del mismo contenedor.

___$ docker container ls___
~~~
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
a7eb68fd6895        ubuntu              "/bin/bash"         21 minutes ago      Up 6 minutes                            gallant_hawking
~~~

___$ docker container top a7eb68fd6895___
~~~
UID                 PID                 PPID                C                   STIME               TTY                 TIME                CMD
root                5495                5476                0                   21:11               pts/0               00:00:00            /bin/bash
root                5802                5476                0                   21:14               pts/1               00:00:00            bash
root                5943                5476                0                   21:16               ?                   00:00:00            bash
~~~

Mostrará los dos proceso que se estan ejecutando del mismo contenedor, en este caso los procesos bash.

### Puertos
___$ docker container run -d nginx___
~~~
0d967185a6e696270049c23b647e0e8882dab5f260a391de4a64555a66f9b01f
~~~
___$ docker container ls___
~~~
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS               NAMES
0d967185a6e6        nginx               "nginx -g 'daemon of…"   26 seconds ago      Up 25 seconds       80/tcp              distracted_grothendieck
~~~
___$ curl localhost___
~~~
curl: (7) Failed to connect to localhost port 80: Connection refused
~~~
___$ docker container stop 0d967185a6e6___
~~~
0d967185a6e6
~~~
___$ docker container ls -q___
~~~
0d967185a6e6
~~~
___$ docker container stop $(docker container ls -q)___
~~~
0d967185a6e6
~~~
___$ docker container ls___
~~~
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
~~~
___$ docker container ls -a___
~~~
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS                      PORTS               NAMES
0d967185a6e6        nginx               "nginx -g 'daemon of…"   4 minutes ago       Exited (0) 3 minutes ago                        distracted_grothendieck
8216742a12ec        nginx               "nginx -g 'daemon of…"   27 minutes ago      Exited (0) 19 minutes ago                       elastic_lovelace
a7eb68fd6895        ubuntu              "/bin/bash"              30 minutes ago      Exited (0) 6 minutes ago                        gallant_hawking
11be7be3bba8        ubuntu              "/bin/bash"              33 minutes ago      Exited (0) 20 minutes ago                       romantic_morse
f39c1b9e6c4d        nginx               "nginx -g 'daemon of…"   36 minutes ago      Exited (0) 33 minutes ago                       eloquent_liskov
b1b5ea9d948e        ubuntu:latest       "/bin/bash"              40 minutes ago      Exited (0) 40 minutes ago                       silly_mclaren
7e653edebf1e        fernando93d/hello   "python ./app.py"        About an hour ago   Exited (0) 45 minutes ago                       trusting_volhard
~~~
___$ docker container prune___
~~~
WARNING! This will remove all stopped containers.
Are you sure you want to continue? [y/N] y
Deleted Containers:
0d967185a6e696270049c23b647e0e8882dab5f260a391de4a64555a66f9b01f
8216742a12ec134095f5990651b2cb0f7865201a5efdf6008e1d6ad4eb401c4e
a7eb68fd6895a514e9e290d24abbaabf0ec96fbc998345ff02f942ff415be6fc
11be7be3bba842f849fe64a88c201d99d101203ee7961457babbd654c482e739
f39c1b9e6c4d0030adc8a896dd1b895f6e81b6a3a0df0580e6a735dde7adfb9a
b1b5ea9d948e56fc79a892fd236f71d5f3531d7f701a6046387a99efc97af0f3
7e653edebf1ed0a1871c8b8c6bf65a27f0856a5ba762d14d959b0d8aab16cc96

Total reclaimed space: 42B
~~~
___$ docker container ls -a___
~~~
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
~~~
___$ docker container run --help___

* -p = publish, sirve para exponer puertos de contenedores hacia nuestro hosts.

___$ docker container run -d -p 80:80 nginx___
~~~
18612da6c30cceb31f5d0abc29f2f15481cc88aceb2c3bcd95807f369f2b5fbc
~~~
El primer 80 es el puerto de nuestro hosts donde veremos el servicio.

El segundo 80 es el puerto donde el contenedor expone el servicio.

___$ docker container ls___
~~~
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                NAMES
18612da6c30c        nginx               "nginx -g 'daemon of…"   35 seconds ago      Up 33 seconds       0.0.0.0:80->80/tcp   elegant_jennings
~~~
___$ curl localhost___
~~~
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>
~~~
___$ docker container run -d -p 8080:80 nginx___
~~~
775c20e7212d3f48bf963406c512551838f5e5cb5e0e20155dd74d27ffba2a77
~~~
___$ docker container ls___
~~~
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                  NAMES
775c20e7212d        nginx               "nginx -g 'daemon of…"   30 seconds ago      Up 28 seconds       0.0.0.0:8080->80/tcp   agitated_kirch
18612da6c30c        nginx               "nginx -g 'daemon of…"   7 minutes ago       Up 7 minutes        0.0.0.0:80->80/tcp     elegant_jennings
~~~
___$ curl localhost:8080___
~~~
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>
~~~
___$ docker container ls___
~~~
CONTAINER ID        IMAGE               COMMAND                  CREATED              STATUS              PORTS                  NAMES
775c20e7212d        nginx               "nginx -g 'daemon of…"   About a minute ago   Up About a minute   0.0.0.0:8080->80/tcp   agitated_kirch
18612da6c30c        nginx               "nginx -g 'daemon of…"   8 minutes ago        Up 8 minutes        0.0.0.0:80->80/tcp     elegant_jennings
~~~
___$ docker container port 775c20e7212d___
~~~
80/tcp -> 0.0.0.0:8080
~~~
___$ docker container run -d -P nginx___
~~~
54d080cd6b0df52f79a65aafaf94462af20c6aa31b55a67c68850413d3131918
~~~
* -P = Docker asignará automáticamente el puerto donde veremos el servicio.

___$ docker container ls___
~~~
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                   NAMES
54d080cd6b0d        nginx               "nginx -g 'daemon of…"   30 seconds ago      Up 29 seconds       0.0.0.0:32768->80/tcp   nervous_darwin
775c20e7212d        nginx               "nginx -g 'daemon of…"   3 minutes ago       Up 3 minutes        0.0.0.0:8080->80/tcp    agitated_kirch
18612da6c30c        nginx               "nginx -g 'daemon of…"   10 minutes ago      Up 10 minutes       0.0.0.0:80->80/tcp      elegant_jennings
~~~
___$ docker container port 54d080cd6b0d___
~~~
80/tcp -> 0.0.0.0:32768
~~~
Suponiendo que entrega el puerto 32768

___$ curl localhost:32768___
~~~
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>
~~~
___$ docker container stop $(docker container ls -q)___
~~~
54d080cd6b0d
775c20e7212d
18612da6c30c
~~~
___$ docker container prune___
~~~
WARNING! This will remove all stopped containers.
Are you sure you want to continue? [y/N] y
Total reclaimed space: 0B
~~~
___$ docker container run -d -p 8080:80 -p 3006:3006 -p 2222:2222 nginx___
~~~
b16b6e41e5ceb8430d9f6d5106a67781131d8a17a8828d85db0319d0298d4cce
~~~
___$ docker container ls___
~~~
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                                                                  NAMES
b16b6e41e5ce        nginx               "nginx -g 'daemon of…"   6 seconds ago       Up 5 seconds        0.0.0.0:2222->2222/tcp, 0.0.0.0:3006->3006/tcp, 0.0.0.0:8080->80/tcp   dazzling_robinson
~~~

___$ docker container port b16b6e41e5ce___
~~~
2222/tcp -> 0.0.0.0:2222
3006/tcp -> 0.0.0.0:3006
80/tcp -> 0.0.0.0:8080
~~~
### Logs
___$ docker container run fernando93d/hello___
~~~
    __  __      __         __  ___                __
   / / / /___  / /___ _   /  |/  /_  ______  ____/ /___
  / /_/ / __ \/ / __ `/  / /|_/ / / / / __ \/ __  / __ \
 / __  / /_/ / / /_/ /  / /  / / /_/ / / / / /_/ / /_/ /
/_/ /_/\____/_/\__,_/  /_/  /_/\__,_/_/ /_/\__,_/\____/
~~~
___$ docker container --help___

___$ docker container ls -a___
~~~
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS                      PORTS               NAMES
4c8fc4f400aa        fernando93d/hello   "python ./app.py"   32 seconds ago      Exited (0) 31 seconds ago                       youthful_colden
~~~
___$ docker container logs 4c8fc4f400aa___
~~~
    __  __      __         __  ___                __
   / / / /___  / /___ _   /  |/  /_  ______  ____/ /___
  / /_/ / __ \/ / __ `/  / /|_/ / / / / __ \/ __  / __ \
 / __  / /_/ / / /_/ /  / /  / / /_/ / / / / /_/ / /_/ /
/_/ /_/\____/_/\__,_/  /_/  /_/\__,_/_/ /_/\__,_/\____/
~~~
___$ docker image pull mysql___
~~~
Using default tag: latest
latest: Pulling from library/mysql
c499e6d256d6: Already exists
22c4cdf4ea75: Pull complete
6ff5091a5a30: Pull complete
2fd3d1af9403: Pull complete
0d9d26127d1d: Pull complete
54a67d4e7579: Pull complete
fe989230d866: Pull complete
3a808704d40c: Pull complete
826517d07519: Pull complete
69cd125db928: Pull complete
b5c43b8c2879: Pull complete
1811572b5ea5: Pull complete
Digest: sha256:b69d0b62d02ee1eba8c7aeb32eba1bb678b6cfa4ccfb211a5d7931c7755dc4a8
Status: Downloaded newer image for mysql:latest
~~~
___$ docker container run -d mysql___
~~~
3d2970ee51413f2dcedaf860f19bc330676ca21913f0dbb0f25b989a51176bab
~~~
___$ docker container ls___
~~~
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
~~~
___$ docker container ls -a___
~~~
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS                      PORTS               NAMES
3d2970ee5141        mysql               "docker-entrypoint.s…"   37 seconds ago      Exited (1) 36 seconds ago                       stupefied_hopper
4c8fc4f400aa        fernando93d/hello   "python ./app.py"        8 minutes ago       Exited (0) 8 minutes ago                        youthful_colden
~~~
___$ docker container logs 3d2970ee5141___
~~~
2020-04-02 04:25:33+00:00 [Note] [Entrypoint]: Entrypoint script for MySQL Server 8.0.19-1debian10 started.
2020-04-02 04:25:33+00:00 [Note] [Entrypoint]: Switching to dedicated user 'mysql'
2020-04-02 04:25:33+00:00 [Note] [Entrypoint]: Entrypoint script for MySQL Server 8.0.19-1debian10 started.
2020-04-02 04:25:33+00:00 [ERROR] [Entrypoint]: Database is uninitialized and password option is not specified
	You need to specify one of MYSQL_ROOT_PASSWORD, MYSQL_ALLOW_EMPTY_PASSWORD and MYSQL_RANDOM_ROOT_PASSWORD
~~~
### Commits
___$ docker container run -dit ubuntu___
~~~
1ac20cc4f3c7ce269d20a4085e216fb6415cdd5ebc3e05c1ed783a171516e38b
~~~
___$ docker container ls___
~~~
1ac20cc4f3c7        ubuntu              "/bin/bash"         14 seconds ago      Up 13 seconds                           focused_edison
~~~
___$ docker container exec -it 1ac20cc4f3c7 bash___
~~~
root@1ac20cc4f3c7:/# touch data.txt
root@1ac20cc4f3c7:/# exit
exit
~~~
Dentro del contenedor crear un archivo "touch data.txt" y salirse con "exit"

___$ docker container --help___

___$ docker container commit --help___

___$ docker container ls___
~~~
CONTAINER ID        IMAGE               COMMAND             CREATED              STATUS              PORTS               NAMES
1ac20cc4f3c7        ubuntu              "/bin/bash"         About a minute ago   Up About a minute                       focused_edison
~~~
___$ docker container commit 1ac20cc4f3c7 ubuntu-file___
~~~
b3f1d74871b68c807a751a9a00dd013d43ea2cba1b7a4ca63beb36be567feaa2
~~~
___$ docker image ls___
~~~
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
ubuntu-file         latest              b3f1d74871b6        16 seconds ago      64.2MB
nginx               latest              ed21b7a8aee9        2 days ago          127MB
mysql               latest              9228ee8bac7a        2 days ago          547MB
ubuntu              latest              4e5021d210f6        12 days ago         64.2MB
fernando93d/hello   latest              7fe934464560        16 months ago       150MB
~~~
___$ docker container run -it ubuntu-file___
~~~
root@9a0ffc257a2c:/# ls
bin  boot  data.txt  dev  etc  home  lib  lib64  media  mnt  opt  proc  root  run  sbin  srv  sys  tmp  usr  var
~~~
## 3. Volúmenes
***
Compartir archivos del host al container o entre varios containers, permitirá almacenar información fuera del contenedor.

### Overview Volúmenes
Que son los volúmenes?

### Creando Volúmenes
___$ docker volume___
~~~
Usage:	docker volume COMMAND

Manage volumes

Commands:
  create      Create a volume
  inspect     Display detailed information on one or more volumes
  ls          List volumes
  prune       Remove all unused local volumes
  rm          Remove one or more volumes

Run 'docker volume COMMAND --help' for more information on a command.
~~~
___$ docker volume --help___

___$ docker volume create --help___

___$ docker volume create local___
~~~
local
~~~
Local es el nombre que le daremos al volumen a crear.

___$ docker volume ls___
~~~
DRIVER              VOLUME NAME
local               local
~~~
___$ docker volume inspect local___
~~~
[
    {
        "CreatedAt": "2020-03-31T21:45:55-06:00",
        "Driver": "local",
        "Labels": {},
        "Mountpoint": "/var/lib/docker/volumes/local/_data",
        "Name": "local",
        "Options": {},
        "Scope": "local"
    }
]
~~~
___$ sudo ls -l /var/lib/docker/volumes/___
~~~
total 24
drwxr-xr-x 3 root root    19 Mar 31 21:45 local
-rw------- 1 root root 32768 Apr  1 23:07 metadata.db
~~~
___$ docker container run -dit -v local:/app ubuntu___
~~~
f29bbcf3c3ee99978bc7ac2a29ebcdee85b9298d992fad262f93a7db57ffa8b3
~~~
___$ docker container exec -it f29bbcf3c3ee bash___
~~~
root@f29bbcf3c3ee:/# ls
app  bin  boot  dev  etc  home  lib  lib64  media  mnt  opt  proc  root  run  sbin  srv  sys  tmp  usr  var
root@f29bbcf3c3ee:/# cd app
root@f29bbcf3c3ee:/app#
root@f29bbcf3c3ee:/app# touch file1 file2
~~~
Dentro del contenedor con el comando "ls" veremos el directorio "/app"

Crear varios archivos dentro del directorio "/app"

Salir del contenedor y eliminarlo

___$ docker container rm f29bbcf3c3ee___
~~~
Error response from daemon: You cannot remove a running container f29bbcf3c3ee99978bc7ac2a29ebcdee85b9298d992fad262f93a7db57ffa8b3. Stop the container before attempting removal or force remove
~~~
No se puede eliminar un contenedor con un volúmen attachado, hay que forzarlo.

___$ docker container rm -f f29bbcf3c3ee___
~~~
f29bbcf3c3ee
~~~
* -f = force

Eliminar todos los contenedores existentes

___$ docker container prune___
~~~
WARNING! This will remove all stopped containers.
Are you sure you want to continue? [y/N] y
Total reclaimed space: 0B
~~~
___$ sudo ls -l /var/lib/docker/volumes/local/_data___
~~~
total 0
-rw-r--r-- 1 root root 0 Apr  1 23:14 file1
-rw-r--r-- 1 root root 0 Apr  1 23:14 file2
~~~

Aquí se encontrarán los archivos que se crearon en el contenedor anterior.

___$ docker container run -it -v local:/src ubuntu___
~~~
root@0d08ad055127:/# ls src
file1  file2
~~~
Dentro del contenedor validar los archivos dentro de "/src".

### Compartiendo archivos con contenedores
___$ docker container run -it -v local:/app ubuntu___
~~~
root@5b4d8db6e9b5:/# ls /app
file1  file2
~~~
En otra terminal crear otro contenedor

___$ docker container run -it -v local:/src ubuntu___
~~~
root@3dc1b4fb0d58:/# ls src
file1  file2

root@5b4d8db6e9b5:/# cd app
root@5b4d8db6e9b5:/app# touch file3
root@5b4d8db6e9b5:/app# ls
file1  file2  file3

root@3dc1b4fb0d58:/# cd src
root@3dc1b4fb0d58:/src# ls
file1  file2  file3
~~~
Validar en ambos contenedores que existan los mismos archivos del volúmen montado.

Crear archivos desde un contenedor se verá reflejado en el otro también.

Compartir un archivo desde local.

___$ mkdir src___

___$ touch src/index.html___

___$ vi src/index.html___
~~~
<h1>Hola Marcos</h1>
~~~
Agregar algún contenido en html para probar.

___$ docker container run -it -v /home/gvillafu/src:/app ubuntu___
~~~
root@a6c10b0d53c3:/# ls app
index.html
root@a6c10b0d53c3:/# touch /app/file4
root@a6c10b0d53c3:/# exit
exit
gvillafu@bonampak:~$ ls src
file4  index.html
~~~
Montará el directorio y su contenido local dentro del contenedor.

Pueden crearse archivos o directorios dentro del contenedor y se verán localmente.

___$ docker container run -d -v /home/gvillafu/src/index.html:/usr/share/nginx/html/index.html -p 80:80 nginx___
~~~
6828f17a798d3bb2b193587713d2a1e2acd3d9bd1212a9abf8e686b0bead74ef
~~~
___$ docker container ls___
~~~
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                NAMES
6828f17a798d        nginx               "nginx -g 'daemon of…"   13 seconds ago      Up 11 seconds       0.0.0.0:80->80/tcp   cranky_pare
5b4d8db6e9b5        ubuntu              "/bin/bash"              9 minutes ago       Up 9 minutes                             nifty_rosalind
~~~
___$ curl localhost:80___
~~~
<h1>Hola Marcos</h1>
~~~
## 4. Imágenes
***
### Overview imágenes

### Dockerfile
___$ vi Dockerfile___

FROM ubuntu

RUN mkdir /app

RUN cd /app && touch data.txt

___$ docker image --help___

___$ docker image build -t ubuntu-file2 .___
~~~
Sending build context to Docker daemon  7.216GB
Step 1/3 : FROM ubuntu
 ---> 4e5021d210f6
Step 2/3 : RUN mkdir /app
 ---> Running in e59aeab2dd06
Removing intermediate container e59aeab2dd06
 ---> 8b0a90d963b0
Step 3/3 : RUN cd /app && touch data.txt
 ---> Running in bc6cf0f5128a
Removing intermediate container bc6cf0f5128a
 ---> 8d327842f158
Successfully built 8d327842f158
Successfully tagged ubuntu-file2:latest
~~~
___$ docker image ls___
~~~
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
ubuntu-file2        latest              8d327842f158        29 minutes ago      64.2MB
ubuntu-file         latest              b3f1d74871b6        About an hour ago   64.2MB
nginx               latest              ed21b7a8aee9        2 days ago          127MB
mysql               latest              9228ee8bac7a        2 days ago          547MB
ubuntu              latest              4e5021d210f6        12 days ago         64.2MB
fernando93d/hello   latest              7fe934464560        16 months ago       150MB
~~~
___$ docker container run -it ubuntu-file2___
~~~
root@986cf4854f6c:/# ls app
data.txt
~~~
Dentro del contenedor en el directorio "/apps" debe existir el archivo "data.txt"

### Copiar archivos desde local
___$ mkdir src___

___$ vi src/data___

Poner algún contenido dentro del archivo y lo salvamos.

___$ vi Dockerfile___

FROM ubuntu

RUN mkdir /app

RUN cd /app && touch data.txt

COPY ./src/data /app/src/com/data.txt

___$ docker image build -t ubuntu-file:v2 .___
~~~
Sending build context to Docker daemon   7.22GB
Step 1/4 : FROM ubuntu
 ---> 4e5021d210f6
Step 2/4 : RUN mkdir /app
 ---> Using cache
 ---> 8b0a90d963b0
Step 3/4 : RUN cd /app && touch data.txt
 ---> Using cache
 ---> 8d327842f158
Step 4/4 : COPY ./src/data /app/src/com/data.txt
 ---> 0ffb0229fc23
Successfully built 0ffb0229fc23
Successfully tagged ubuntu-file:v2
~~~
___$ docker container run -it ubuntu-file:v2 bash___
~~~
root@089495cf34e9:/# ls
app  bin  boot  dev  etc  home  lib  lib64  media  mnt  opt  proc  root  run  sbin  srv  sys  tmp  usr  var
root@089495cf34e9:/# ls app/
data.txt  src/
root@089495cf34e9:/# ls app/src/com/
data.txt
root@089495cf34e9:/# cat app/src/com/data.txt
Contenido del archivo data
~~~
Dentro del contenedor en la ruta "/app/src/com/" estará el archivo con su contenido.

___$ docker container run --rm -it ubuntu-file:v2___
~~~
root@9f17bbf31edc:/# cat app/src/com/data.txt
Contenido del archivo data
~~~
___$ vi Dockerfile___

FROM ubuntu

RUN mkdir /app

RUN cd /app && touch data.txt

COPY ./src /app/src

___$ docker image build -t ubuntu-file:v3 .___
~~~
Sending build context to Docker daemon  7.214GB
Step 1/4 : FROM ubuntu
 ---> 4e5021d210f6
Step 2/4 : RUN mkdir /app
 ---> Using cache
 ---> 8b0a90d963b0
Step 3/4 : RUN cd /app && touch data.txt
 ---> Using cache
 ---> 8d327842f158
Step 4/4 : COPY ./src /app/src
 ---> 2e7ff8e0f787
Successfully built 2e7ff8e0f787
Successfully tagged ubuntu-file:v3
~~~
___$ docker container run --rm -it ubuntu-file:v3___
~~~
root@01ecae549df4:/# ls app/src/
data  file4  index.html
~~~
Validar el contenido dentro del contenedor.

___$ vi Dockerfile___

FROM ubuntu

RUN mkdir /app

RUN cd /app && touch data.txt

COPY ./src /app/src

ADD file.tar.gz /com/src

___$ docker image build -t ubuntu-file:v4 .___
~~~
Sending build context to Docker daemon  7.225GB
Step 1/5 : FROM ubuntu
 ---> 4e5021d210f6
Step 2/5 : RUN mkdir /app
 ---> Using cache
 ---> 8b0a90d963b0
Step 3/5 : RUN cd /app && touch data.txt
 ---> Using cache
 ---> 8d327842f158
Step 4/5 : COPY ./src /app/src
 ---> Using cache
 ---> 2e7ff8e0f787
Step 5/5 : ADD file.tar.gz /com/src
 ---> 5049755336f5
Successfully built 5049755336f5
Successfully tagged ubuntu-file:v4
~~~
___$ docker container run --rm -it ubuntu-file:v4___
~~~
root@9e79f6e53340:/# ls com/src/
image.png
root@9e79f6e53340:/#
~~~
Validar el contenido dentro del contenedor.

### Variables de entorno
___echo $USER___
~~~
gvillafu
~~~

___$ docker container run --rm -it -e USER=$USER ubuntu___
~~~
root@78a3d285913f:/# id
uid=0(root) gid=0(root) groups=0(root)
root@78a3d285913f:/# echo $USER
gvillafu
root@78a3d285913f:/# id
uid=0(root) gid=0(root) groups=0(root)
~~~
Dentro del contenedor validar la variable $USER.

___$ vi Dockerfile___

FROM ubuntu

ENV USER gvillafu

RUN mkdir /app

RUN cd /app && touch data.txt

COPY ./src /app/src

ADD file.tar.gz /com/src

___$ docker image build -t ubuntu-file:v5 .___
~~~
Sending build context to Docker daemon  7.061GB
Step 1/6 : FROM ubuntu
 ---> 4e5021d210f6
Step 2/6 : ENV USER gvillafu
 ---> Running in aa7908577faa
Removing intermediate container aa7908577faa
 ---> 6b476e4f1d99
Step 3/6 : RUN mkdir /app
 ---> Running in e62c62849808
Removing intermediate container e62c62849808
 ---> d1958f3e0cb9
Step 4/6 : RUN cd /app && touch data.txt
 ---> Running in 3f008bebf24e
Removing intermediate container 3f008bebf24e
 ---> c5e423678181
Step 5/6 : COPY ./src /app/src
 ---> 27bde894973d
Step 6/6 : ADD file.tar.gz /com/src
 ---> 228dc2dba792
Successfully built 228dc2dba792
Successfully tagged ubuntu-file:v5
~~~
___$ docker container run --rm -it ubuntu-file:v5___
~~~
root@7e0363c8e55f:/# echo $USER
gvillafu
~~~
Dentro del contenedor validar la variable $USER.

___$ docker container run --rm -it -e USER="Marcos" ubuntu-file:v5___
~~~
root@d04f51cefc9d:/# echo $USER
Marcos
~~~
Las variables dentro del contenedor son reemplazables con el parámetro "-e"

* -e = env list

Dentro del contenedor validar la variable $USER.

### Argumentos
___$ vi Dockerfile___

ARG DISTRO="ubuntu:18.04"

FROM ${DISTRO}

ENV USER gvillafu

RUN mkdir /app

RUN cd /app && touch data.txt

COPY ./src /app/src

ADD file.tar.gz /com/src

___$ docker image build -t ubuntu-file:v6 .___
~~~
Sending build context to Docker daemon  7.078GB
Step 1/7 : ARG DISTRO="ubuntu:18.04"
Step 2/7 : FROM ${DISTRO}
18.04: Pulling from library/ubuntu
Digest: sha256:bec5a2727be7fff3d308193cfde3491f8fba1a2ba392b7546b43a051853a341d
Status: Downloaded newer image for ubuntu:18.04
 ---> 4e5021d210f6
Step 3/7 : ENV USER gvillafu
 ---> Using cache
 ---> 6b476e4f1d99
Step 4/7 : RUN mkdir /app
 ---> Using cache
 ---> d1958f3e0cb9
Step 5/7 : RUN cd /app && touch data.txt
 ---> Using cache
 ---> c5e423678181
Step 6/7 : COPY ./src /app/src
 ---> Using cache
 ---> 27bde894973d
Step 7/7 : ADD file.tar.gz /com/src
 ---> Using cache
 ---> 228dc2dba792
Successfully built 228dc2dba792
Successfully tagged ubuntu-file:v6
~~~
___$ docker image build -t ubuntu-file:v7 --build-arg DISTRO=ubuntu:19.04 .___
~~~
Sending build context to Docker daemon   7.09GB
Step 1/7 : ARG DISTRO="ubuntu:18.04"
Step 2/7 : FROM ${DISTRO}
19.04: Pulling from library/ubuntu
4dc9c2fff018: Pull complete
0a4ccbb24215: Pull complete
c0f243bc6706: Pull complete
5ff1eaecba77: Pull complete
Digest: sha256:2adeae829bf27a3399a0e7db8ae38d5adb89bcaf1bbef378240bc0e6724e8344
Status: Downloaded newer image for ubuntu:19.04
 ---> c88ac1f841b7
Step 3/7 : ENV USER gvillafu
 ---> Running in bfd69d0c1bef
Removing intermediate container bfd69d0c1bef
 ---> dae89f3b3151
Step 4/7 : RUN mkdir /app
 ---> Running in deb93a030874
Removing intermediate container deb93a030874
 ---> 0dee2e21a137
Step 5/7 : RUN cd /app && touch data.txt
 ---> Running in 6bc0005a5037
Removing intermediate container 6bc0005a5037
 ---> 08de471e1188
Step 6/7 : COPY ./src /app/src
 ---> d495af435d9e
Step 7/7 : ADD file.tar.gz /com/src
 ---> 0bfe37107bf9
Successfully built 0bfe37107bf9
Successfully tagged ubuntu-file:v7
~~~
Los argumentos pueden ser reemplazados cuando se genera la imagen.

___$ docker container run --rm -it ubuntu-file:v7 bash___
~~~
root@d3ab0c145102:/# cat /etc/lsb-release
DISTRIB_ID=Ubuntu
DISTRIB_RELEASE=19.04
DISTRIB_CODENAME=disco
DISTRIB_DESCRIPTION="Ubuntu 19.04"
~~~

### Dockerignore
___$ vi .dockerignore___

./src/a
./src/b

./src/*.conf

Los archivos que se llaman "a", "b" y todos aquellos con extensión "*.conf" serán ignorados cuando se cree la imagen. Esto no se define dentro del Dockerfile pero es importante que ".dockerignore" se encuentre en la misma ubicación que el Dockerfile.

___$ docker image build -t ubuntu-file:v8 .___
~~~
Sending build context to Docker daemon  7.142GB
Step 1/7 : ARG DISTRO="ubuntu:18.04"
Step 2/7 : FROM ${DISTRO}
 ---> 4e5021d210f6
Step 3/7 : ENV USER gvillafu
 ---> Using cache
 ---> 6b476e4f1d99
Step 4/7 : RUN mkdir /app
 ---> Using cache
 ---> d1958f3e0cb9
Step 5/7 : RUN cd /app && touch data.txt
 ---> Using cache
 ---> c5e423678181
Step 6/7 : COPY ./src /app/src
 ---> Using cache
 ---> 27bde894973d
Step 7/7 : ADD file.tar.gz /com/src
 ---> Using cache
 ---> 228dc2dba792
Successfully built 228dc2dba792
Successfully tagged ubuntu-file:v8
~~~
___$ docker container run --rm -it ubuntu-file:v8___
~~~
root@4c55a0c459c6:/# ls app/src/
data  file4  index.html
~~~
Validar dentro del contenedor que no se encuentren los archivos ignorados.

### Trabajando con usuarios
No se recomienda trabajar con usuario root aunque eso depende de que se este ejecutando.

___$ vi Dockerfile___

ARG DISTRO="ubuntu:18.04"

FROM ${DISTRO}

RUN useradd -ms /bin/bash cloud_user

RUN mkdir /app

RUN cd /app && touch data.txt

COPY ./src /app/src

USER cloud_user

RUN cd /home/cloud_user && touch data.txt

TODO lo que se hace después de la directiva "USER" se hace ya con el usuario que se definio.

___$ docker image build -t ubuntu-file:v9 .___
~~~
Sending build context to Docker daemon  7.157GB
Step 1/8 : ARG DISTRO="ubuntu:18.04"
Step 2/8 : FROM ${DISTRO}
 ---> 4e5021d210f6
Step 3/8 : RUN useradd -ms /bin/bash cloud_user
 ---> Running in f1fd8aedc3d3
Removing intermediate container f1fd8aedc3d3
 ---> e6cab2f9ec39
Step 4/8 : RUN mkdir /app
 ---> Running in 2f9ecbeada54
Removing intermediate container 2f9ecbeada54
 ---> cbf112f44267
Step 5/8 : RUN cd /app && touch data.txt
 ---> Running in 66263c40950d
Removing intermediate container 66263c40950d
 ---> 792710728493
Step 6/8 : COPY ./src /app/src
 ---> 32593825dbb1
Step 7/8 : USER cloud_user
 ---> Running in 65bb65dd2b92
Removing intermediate container 65bb65dd2b92
 ---> 82863b7fcdfb
Step 8/8 : RUN cd /home/cloud_user && touch data.txt
 ---> Running in f108306989a3
Removing intermediate container f108306989a3
 ---> 7b308ae55a65
Successfully built 7b308ae55a65
Successfully tagged ubuntu-file:v9
~~~
___$ docker container run --rm -it ubuntu-file:v9___
~~~
cloud_user@f39abdfdf654:/$ ls -l home/cloud_user/
total 0
-rw-r--r-- 1 cloud_user cloud_user 0 Apr  2 07:03 data.txt
cloud_user@f39abdfdf654:/$
~~~
Al entrar al contenedor el usuario debe ser "cloud_user" y no "root". Validar dentro del contenedor que se haya creado el archivo con el usuario "cloud_user". Cada vez que se entre al contenedor siempre será con el usuario "cloud_user".

___$ docker container exec --help___

___$ docker container run -it ubuntu-file:v9___
~~~
cloud_user@3f71be0b178b:/$ exit
exit
~~~
___$ docker container exec -u 0 -it 3f71be0b178b bash___
~~~
$ docker container start 3f71be0b178b
3f71be0b178b

$ docker container exec -u 0 -it 3f71be0b178b bash
root@3f71be0b178b:/# id
uid=0(root) gid=0(root) groups=0(root)
~~~
Se ejecutará el contenedor con el "USER_ID 0" que pertenece a "root". Al entrar al contenedor veremos que se esta ejecutando como usuario "root".

### Ejecutando comandos CMD
___$ vi Dockerfile___

FROM ubuntu

CMD ["echo","Hola Mundo"]

___$ docker image build -t cmd .___
~~~
Sending build context to Docker daemon  7.214GB
Step 1/2 : FROM ubuntu
 ---> 4e5021d210f6
Step 2/2 : CMD ["echo","Hola Mundo"]
 ---> Running in 2ee5e5746c2a
Removing intermediate container 2ee5e5746c2a
 ---> 99ceb653e3a8
Successfully built 99ceb653e3a8
Successfully tagged cmd:latest
~~~
___$ docker container run cmd___
~~~
Hola Mundo
~~~
Al ejecutar el contenedor debe mostrarse la salida "Hola Mundo".

### CMD vs ENTRYPOINT
___$ docker container run cmd echo "Hola Marcos"___
~~~
Hola Marcos
~~~
___$ docker container ls -a___
~~~
CONTAINER ID        IMAGE               COMMAND                CREATED             STATUS                         PORTS               NAMES
0795d8526f1a        cmd                 "echo 'Hola Marcos'"   20 seconds ago      Exited (0) 17 seconds ago                          mystifying_haibt
9f6be9a7ee01        cmd                 "echo 'Hola Mundo'"    59 seconds ago      Exited (0) 57 seconds ago                          flamboyant_minsky
3f71be0b178b        ubuntu-file:v9      "/bin/bash"            9 minutes ago       Up 6 minutes                                       clever_morse
089495cf34e9        ubuntu-file:v2      "bash"                 About an hour ago   Exited (0) About an hour ago                       serene_chatterjee
986cf4854f6c        ubuntu-file2        "/bin/bash"            About an hour ago   Exited (0) About an hour ago
~~~
Se verá que en el último contenedor no se ejecutó "Hola Mundo" sino que se ejecutó "Hola Marcos".

Todo lo que se ejecuta con la directiva CMD es reemplazable.

___$ vi Dockerfile___

FROM ubuntu

ENTRYPOINT ["echo","Hola Mundo"]

___$ docker image build -t cmd2 .___
~~~
Sending build context to Docker daemon  7.213GB
Step 1/2 : FROM ubuntu
 ---> 4e5021d210f6
Step 2/2 : ENTRYPOINT ["echo","Hola Mundo"]
 ---> Running in 378f19a3d567
Removing intermediate container 378f19a3d567
 ---> f0eb1d1d42e0
Successfully built f0eb1d1d42e0
Successfully tagged cmd2:latest
~~~
___$ docker container run cmd2___
~~~
Hola Mundo
~~~
___$ docker container run cmd2 "Hola Marcos"
~~~
Hola Mundo Hola Marcos
~~~
En la ejecución del docker se veran ambas salidas.

___$ vi Dockerfile___

FROM ubuntu

ENTRYPOINT ["echo"]
CMD ["Hola"]

___$ docker image build -t cmd3 .___
~~~
Sending build context to Docker daemon  7.214GB
Step 1/3 : FROM ubuntu
 ---> 4e5021d210f6
Step 2/3 : ENTRYPOINT ["echo"]
 ---> Running in 6b6cc3ae2e68
Removing intermediate container 6b6cc3ae2e68
 ---> eaf76877d672
Step 3/3 : CMD ["Hola"]
 ---> Running in eada5c24744f
Removing intermediate container eada5c24744f
 ---> 84a5e32db85a
Successfully built 84a5e32db85a
Successfully tagged cmd3:latest
~~~
___$ docker container run cmd3___
~~~
Hola
~~~
En la ejecución del contenedor solo se verá la salida "Hola". En este caso CMD sirve como parámetro de ENTRYPOINT.

___$ docker container ps -a___
~~~
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS                         PORTS               NAMES
5b66bd31b032        cmd3                "echo Hola"              27 seconds ago      Exited (0) 25 seconds ago                          friendly_saha
74e794517764        cmd2                "echo 'Hola Mundo' '…"   2 minutes ago       Exited (0) 2 minutes ago                           relaxed_bohr
30eb16ec90e3        cmd2                "echo 'Hola Mundo' '…"   2 minutes ago       Exited (0) 2 minutes ago                           heuristic_meninsky
a1661370ae76        cmd2                "echo 'Hola Mundo' H…"   3 minutes ago       Exited (0) 3 minutes ago                           gallant_blackburn
0bcb64e06f2b        cmd2                "echo 'Hola Mundo' e…"   4 minutes ago       Exited (0) 3 minutes ago                           romantic_noether
7613bbe372ca        cmd2                "echo 'Hola Mundo'"      4 minutes ago       Exited (0) 4 minutes ago                           adoring_bartik
0795d8526f1a        cmd                 "echo 'Hola Marcos'"     7 minutes ago       Exited (0) 7 minutes ago                           mystifying_haibt
9f6be9a7ee01        cmd                 "echo 'Hola Mundo'"      8 minutes ago       Exited (0) 8 minutes ago                           flamboyant_minsky
3f71be0b178b        ubuntu-file:v9      "/bin/bash"              17 minutes ago      Up 14 minutes                                      clever_morse
089495cf34e9        ubuntu-file:v2      "bash"                   About an hour ago   Exited (0) About an hour ago                       serene_chatterjee
986cf4854f6c        ubuntu-file2        "/bin/bash"              About an hour ago   Exited (0) About an hour ago                       adoring_bardeen

~~~
### Contenerizar APP
___$ mkdir container && cd container___

___$ vi index.js___

const express = require('express')
const app = express()

app.get('/', (req, res) => res.send('Hola Mundo'))
app.listen(3000, () => console.log("Hola"))

___$ docker image pull node___
~~~
Using default tag: latest
latest: Pulling from library/node
56da78ce36e9: Pull complete
fbfe0f13ac45: Pull complete
6254ff6d0e60: Pull complete
e0e1e13bd9f6: Pull complete
b86b38b40a24: Pull complete
e357e1a6c1b2: Pull complete
f388b7dd520c: Pull complete
fd75ebfa3fbd: Pull complete
e71a5faa678a: Pull complete
Digest: sha256:c9f3ef4453d9f528c7e35261ad68935867ab524f9f735c457141dc8efb3dfd14
Status: Downloaded newer image for node:latest
~~~
___$ docker container run --help___

___$ docker container run --rm -w /app -v /home/gvillaf/container:/app -it node___
~~~
Welcome to Node.js v13.12.0.
Type ".help" for more information.
> console.log("hola")
hola
undefined
>
~~~
___$ docker container run --rm -w /app -v /home/gvillaf/container:/app -it node bash___
~~~
root@78a7349bc1ea:/app# pwd
/app
root@78a7349bc1ea:/app# ls
~~~
Al ejecutar el contenedor entrará directamente al directorio "/app".

___$ docker container run --rm -w /app -v /home/gvillaf/container:/app -it node npm init -y___
~~~
Wrote to /app/package.json:

{
  "name": "app",
  "version": "1.0.0",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "keywords": [],
  "author": "",
  "license": "ISC",
  "description": ""
}
~~~
En el directorio /app del contenedor se encontrará el archivo "package.json" con la estructura necesaria para la ejecución de "index.js".

___$ docker container run --rm -w /app -v /home/gvillaf/container:/app -it node bash___
~~~
root@c8042ff0d928:/app# ls
package.json
~~~
___$ cat package.json___
~~~
root@c8042ff0d928:/app# cat package.json
{
  "name": "app",
  "version": "1.0.0",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "keywords": [],
  "author": "",
  "license": "ISC",
  "description": ""
}
~~~
___$ docker container run --rm -w /app -v /home/gvillaf/container:/app -it node npm install -S express___
~~~
npm notice created a lockfile as package-lock.json. You should commit this file.
npm WARN app@1.0.0 No description
npm WARN app@1.0.0 No repository field.

+ express@4.17.1
added 50 packages from 37 contributors and audited 126 packages in 3.401s
found 0 vulnerabilities
~~~
Dentro del directorio actual se verán varios archivos y directorios creados producto del último contenedor ejecutado.

___$ docker container run --rm -w /app -v /home/gvillaf/container:/app -it node npm install -S express___
~~~
npm notice created a lockfile as package-lock.json. You should commit this file.
npm WARN app@1.0.0 No description
npm WARN app@1.0.0 No repository field.

+ express@4.17.1
added 50 packages from 37 contributors and audited 126 packages in 3.401s
found 0 vulnerabilities
~~~
___$ ls___
~~~
node_modules  package-lock.json  package.json
~~~
___$ rm -r node_modules/___
~~~
root@43c422ceb116:/app# rm -r node_modules/
root@43c422ceb116:/app#
~~~
___$ cat package.json___
~~~
{
  "name": "app",
  "version": "1.0.0",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "keywords": [],
  "author": "",
  "license": "ISC",
  "description": "",
  "dependencies": {
    "express": "^4.17.1"
  }
}
~~~
Deberá ya existir la dependencia "express".

___$ vi Dockerfile___

FROM node

WORKDIR /app

COPY package.json .

RUN npm install

COPY index.js .

CMD ["node","index.js"]

___$ docker image build -t web .___
~~~
Sending build context to Docker daemon  4.096kB
Step 1/6 : FROM node
 ---> c31fbeb964cc
Step 2/6 : WORKDIR /app
 ---> Using cache
 ---> 188f534e9cb6
Step 3/6 : COPY package.json .
 ---> a84abd06ed3a
Step 4/6 : RUN npm install
 ---> Running in 76bad1c7c8b0
npm notice created a lockfile as package-lock.json. You should commit this file.
npm WARN app@1.0.0 No description
npm WARN app@1.0.0 No repository field.

added 50 packages from 37 contributors and audited 126 packages in 9.594s
found 0 vulnerabilities

Removing intermediate container 76bad1c7c8b0
 ---> 1c0354f8296f
Step 5/6 : COPY index.js .
 ---> 165083b6cf18
Step 6/6 : CMD ["node","index.js"]
 ---> Running in 6bf2f1cdc3c7
Removing intermediate container 6bf2f1cdc3c7
 ---> 0f90fa152c4a
Successfully built 0f90fa152c4a
Successfully tagged web:latest
~~~
___$ docker image ls___
~~~
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
web                 latest              0f90fa152c4a        24 seconds ago      946MB
cmd3                latest              84a5e32db85a        30 minutes ago      64.2MB
cmd2                latest              f0eb1d1d42e0        34 minutes ago      64.2MB
cmd                 latest              99ceb653e3a8        38 minutes ago      64.2MB
ubuntu-file         v9                  7b308ae55a65        About an hour ago   64.6MB
ubuntu-file         v7                  0bfe37107bf9        About an hour ago   70.1MB
ubuntu-file         v5                  228dc2dba792        About an hour ago   64.3MB
ubuntu-file         v6                  228dc2dba792        About an hour ago   64.3MB
ubuntu-file         v8                  228dc2dba792        About an hour ago   64.3MB
ubuntu-file         v4                  5049755336f5        About an hour ago   64.3MB
ubuntu-file         v3                  2e7ff8e0f787        About an hour ago   64.2MB
ubuntu-file         v2                  0ffb0229fc23        2 hours ago         64.2MB
ubuntu-file2        latest              8d327842f158        2 hours ago         64.2MB
ubuntu-file         latest              b3f1d74871b6        3 hours ago         64.2MB
node                latest              c31fbeb964cc        2 days ago          943MB
nginx               latest              ed21b7a8aee9        2 days ago          127MB
mysql               latest              9228ee8bac7a        2 days ago          547MB
ubuntu              18.04               4e5021d210f6        12 days ago         64.2MB
ubuntu              latest              4e5021d210f6        12 days ago         64.2MB
ubuntu              19.04               c88ac1f841b7        2 months ago        70MB
fernando93d/hello   latest              7fe934464560        16 months ago       150MB
~~~
___$ docker container run -d -p 3000:3000 web___
~~~
f61611bc7ea37fe5f535afa927a1d849522223eadd66e0d76be0dc2954ee0cdb
~~~
___$ docker ps___
~~~
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                    NAMES
f61611bc7ea3        web                 "docker-entrypoint.s…"   12 seconds ago      Up 11 seconds       0.0.0.0:3000->3000/tcp   keen_chatterjee
3f71be0b178b        ubuntu-file:v9      "/bin/bash"              About an hour ago   Up 44 minutes                                clever_morse
~~~
___$ curl localhost:3000___
~~~
Hola Mundo
~~~
### Subir imagen al docker hub
___$ docker login___
~~~
$ docker login
Login with your Docker ID to push and pull images from Docker Hub. If you don't have a Docker ID, head over to https://hub.docker.com to create one.
Username: mgvillaf
Password:
Error saving credentials: error storing credentials - err: exit status 1, out: `Failed to execute child process “dbus-launch” (No such file or directory)`

gvillafu@bonampak:~/container$ install gnupg2 pass

gvillafu@bonampak:~/container$ docker login
Login with your Docker ID to push and pull images from Docker Hub. If you don't have a Docker ID, head over to https://hub.docker.com to create one.
Username: mgvillaf
Password:
WARNING! Your password will be stored unencrypted in /root/.docker/config.json.
Configure a credential helper to remove this warning. See
https://docs.docker.com/engine/reference/commandline/login/#credentials-store

Login Succeeded

~~~
___$ docker image ls___
~~~
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
web                 latest              0f90fa152c4a        12 minutes ago      946MB
cmd3                latest              84a5e32db85a        42 minutes ago      64.2MB
cmd2                latest              f0eb1d1d42e0        About an hour ago   64.2MB
cmd                 latest              99ceb653e3a8        About an hour ago   64.2MB
ubuntu-file         v9                  7b308ae55a65        About an hour ago   64.6MB
ubuntu-file         v7                  0bfe37107bf9        About an hour ago   70.1MB
ubuntu-file         v5                  228dc2dba792        About an hour ago   64.3MB
ubuntu-file         v6                  228dc2dba792        About an hour ago   64.3MB
ubuntu-file         v8                  228dc2dba792        About an hour ago   64.3MB
ubuntu-file         v4                  5049755336f5        2 hours ago         64.3MB
ubuntu-file         v3                  2e7ff8e0f787        2 hours ago         64.2MB
ubuntu-file         v2                  0ffb0229fc23        2 hours ago         64.2MB
ubuntu-file2        latest              8d327842f158        3 hours ago         64.2MB
ubuntu-file         latest              b3f1d74871b6        3 hours ago         64.2MB
node                latest              c31fbeb964cc        2 days ago          943MB
nginx               latest              ed21b7a8aee9        2 days ago          127MB
mysql               latest              9228ee8bac7a        2 days ago          547MB
ubuntu              18.04               4e5021d210f6        12 days ago         64.2MB
ubuntu              latest              4e5021d210f6        12 days ago         64.2MB
ubuntu              19.04               c88ac1f841b7        2 months ago        70MB
fernando93d/hello   latest              7fe934464560        16 months ago       150MB
~~~
___$ docker push --help___

___$ docker push web___
~~~
The push refers to repository [docker.io/library/web]
4cdcda036516: Preparing
104a2956964e: Preparing
7363841280f5: Preparing
6aabf8d5adc1: Preparing
c058eaf748c8: Preparing
eb58d2440516: Waiting
66cf06b2e874: Waiting
45ac74adb5b4: Waiting
d485cbbe6a5e: Waiting
391c89959588: Waiting
588545a7a2a3: Waiting
8452468a5e50: Waiting
55b19a5e648f: Waiting
denied: requested access to the resource is denied
~~~
Aparecerá un error de que la petición ha sido denegada.

___$ docker tag --help___

___$ docker tag web mgvillaf/node_app:v1___

___$ docker image ls___
~~~
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
web                 latest              0f90fa152c4a        14 minutes ago      946MB
mgvillaf/node_app   v1                  0f90fa152c4a        14 minutes ago      946MB
cmd3                latest              84a5e32db85a        44 minutes ago      64.2MB
cmd2                latest              f0eb1d1d42e0        About an hour ago   64.2MB
cmd                 latest              99ceb653e3a8        About an hour ago   64.2MB
ubuntu-file         v9                  7b308ae55a65        About an hour ago   64.6MB
ubuntu-file         v7                  0bfe37107bf9        About an hour ago   70.1MB
ubuntu-file         v5                  228dc2dba792        About an hour ago   64.3MB
ubuntu-file         v6                  228dc2dba792        About an hour ago   64.3MB
ubuntu-file         v8                  228dc2dba792        About an hour ago   64.3MB
ubuntu-file         v4                  5049755336f5        2 hours ago         64.3MB
ubuntu-file         v3                  2e7ff8e0f787        2 hours ago         64.2MB
ubuntu-file         v2                  0ffb0229fc23        2 hours ago         64.2MB
ubuntu-file2        latest              8d327842f158        3 hours ago         64.2MB
ubuntu-file         latest              b3f1d74871b6        3 hours ago         64.2MB
node                latest              c31fbeb964cc        2 days ago          943MB
nginx               latest              ed21b7a8aee9        2 days ago          127MB
mysql               latest              9228ee8bac7a        2 days ago          547MB
ubuntu              18.04               4e5021d210f6        12 days ago         64.2MB
ubuntu              latest              4e5021d210f6        12 days ago         64.2MB
ubuntu              19.04               c88ac1f841b7        2 months ago        70MB
fernando93d/hello   latest              7fe934464560        16 months ago       150MB
~~~
___$ docker push mgvillaf/node_app:v1___
~~~
The push refers to repository [docker.io/mgvillaf/node_app]
4cdcda036516: Pushed
104a2956964e: Pushed
7363841280f5: Pushed
6aabf8d5adc1: Pushed
c058eaf748c8: Pushed
eb58d2440516: Pushed
66cf06b2e874: Pushed
45ac74adb5b4: Pushed
d485cbbe6a5e: Pushed
391c89959588: Pushed
588545a7a2a3: Pushed
8452468a5e50: Pushed
55b19a5e648f: Pushed
v1: digest: sha256:d937314f1bb64e63916159d132bffc27abd91aa54b024695776020a033a6a810 size: 3047
~~~
Revisar en docker hub con nuestro usuario y la imagen ya debe existir.

## 5. Redes
***
### Redes en Docker
1. Bridge

2. Host

3. None

### Inspeccionando Redes
___$ docker network___
~~~
Usage:	docker network COMMAND

Manage networks

Commands:
  connect     Connect a container to a network
  create      Create a network
  disconnect  Disconnect a container from a network
  inspect     Display detailed information on one or more networks
  ls          List networks
  prune       Remove all unused networks
  rm          Remove one or more networks

Run 'docker network COMMAND --help' for more information on a command.
~~~
___$ docker network ls___
~~~
NETWORK ID          NAME                DRIVER              SCOPE
be2cda653509        bridge              bridge              local
ef2473a96188        host                host                local
02234d7f44de        none                null                local
~~~
___$ docker network inspect bridge___
~~~
[
    {
        "Name": "bridge",
        "Id": "be2cda653509c6435ec33c40e2b7e50d206af09c99cf7943ea7ef62a9e040888",
        "Created": "2020-04-01T10:50:27.538671636-06:00",
        "Scope": "local",
        "Driver": "bridge",
        "EnableIPv6": false,
        "IPAM": {
            "Driver": "default",
            "Options": null,
            "Config": [
                {
                    "Subnet": "172.17.0.0/16",
                    "Gateway": "172.17.0.1"
                }
            ]
        },
        "Internal": false,
        "Attachable": false,
        "Ingress": false,
        "ConfigFrom": {
            "Network": ""
        },
        "ConfigOnly": false,
        "Containers": {
            "3f71be0b178b1a01bd74b6bb5a4bbe99a91c2c05d7d5b87ed6f94b10e4af7bcc": {
                "Name": "clever_morse",
                "EndpointID": "609e0a154407d76fb9a61074b9d857a3df0ff52f95f0e8b90afd367267e98768",
                "MacAddress": "02:42:ac:11:00:02",
                "IPv4Address": "172.17.0.2/16",
                "IPv6Address": ""
            },
            "f61611bc7ea37fe5f535afa927a1d849522223eadd66e0d76be0dc2954ee0cdb": {
                "Name": "keen_chatterjee",
                "EndpointID": "855dd42afa89f926ff769fcb886331eb985a145f1ae2b38ef830a58d07b28136",
                "MacAddress": "02:42:ac:11:00:03",
                "IPv4Address": "172.17.0.3/16",
                "IPv6Address": ""
            }
        },
        "Options": {
            "com.docker.network.bridge.default_bridge": "true",
            "com.docker.network.bridge.enable_icc": "true",
            "com.docker.network.bridge.enable_ip_masquerade": "true",
            "com.docker.network.bridge.host_binding_ipv4": "0.0.0.0",
            "com.docker.network.bridge.name": "docker0",
            "com.docker.network.driver.mtu": "1500"
        },
        "Labels": {}
    }
]
~~~
___$ docker container run -dit ubuntu___
~~~
a21fc4f1b9afff09e6b08b79e923e5bccb06b81f735179762ee0be56c46337a6
~~~
___$ docker container ls___
~~~
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                    NAMES
a21fc4f1b9af        ubuntu              "/bin/bash"              16 seconds ago      Up 14 seconds                                frosty_turing
f61611bc7ea3        web                 "docker-entrypoint.s…"   30 minutes ago      Up 30 minutes       0.0.0.0:3000->3000/tcp   keen_chatterjee
3f71be0b178b        ubuntu-file:v9      "/bin/bash"              About an hour ago   Up About an hour                             clever_morse
~~~
___$ docker network inspect bridge___
~~~
[
    {
        "Name": "bridge",
        "Id": "be2cda653509c6435ec33c40e2b7e50d206af09c99cf7943ea7ef62a9e040888",
        "Created": "2020-04-01T10:50:27.538671636-06:00",
        "Scope": "local",
        "Driver": "bridge",
        "EnableIPv6": false,
        "IPAM": {
            "Driver": "default",
            "Options": null,
            "Config": [
                {
                    "Subnet": "172.17.0.0/16",
                    "Gateway": "172.17.0.1"
                }
            ]
        },
        "Internal": false,
        "Attachable": false,
        "Ingress": false,
        "ConfigFrom": {
            "Network": ""
        },
        "ConfigOnly": false,
        "Containers": {
            "3f71be0b178b1a01bd74b6bb5a4bbe99a91c2c05d7d5b87ed6f94b10e4af7bcc": {
                "Name": "clever_morse",
                "EndpointID": "609e0a154407d76fb9a61074b9d857a3df0ff52f95f0e8b90afd367267e98768",
                "MacAddress": "02:42:ac:11:00:02",
                "IPv4Address": "172.17.0.2/16",
                "IPv6Address": ""
            },
            "a21fc4f1b9afff09e6b08b79e923e5bccb06b81f735179762ee0be56c46337a6": {
                "Name": "frosty_turing",
                "EndpointID": "e1dc0ef83c09b1086b4f647fcd47e6dcca84eba5e8b4c4aaf8f7c0f331ce7bb1",
                "MacAddress": "02:42:ac:11:00:04",
                "IPv4Address": "172.17.0.4/16",
                "IPv6Address": ""
            },
            "f61611bc7ea37fe5f535afa927a1d849522223eadd66e0d76be0dc2954ee0cdb": {
                "Name": "keen_chatterjee",
                "EndpointID": "855dd42afa89f926ff769fcb886331eb985a145f1ae2b38ef830a58d07b28136",
                "MacAddress": "02:42:ac:11:00:03",
                "IPv4Address": "172.17.0.3/16",
                "IPv6Address": ""
            }
        },
        "Options": {
            "com.docker.network.bridge.default_bridge": "true",
            "com.docker.network.bridge.enable_icc": "true",
            "com.docker.network.bridge.enable_ip_masquerade": "true",
            "com.docker.network.bridge.host_binding_ipv4": "0.0.0.0",
            "com.docker.network.bridge.name": "docker0",
            "com.docker.network.driver.mtu": "1500"
        },
        "Labels": {}
    }
]
~~~
Deberán verse los clientes que pertenecen a esta red.

___$ docker container inspect a21fc4f1b9af___
~~~
[
    {
        "Id": "a21fc4f1b9afff09e6b08b79e923e5bccb06b81f735179762ee0be56c46337a6",
        "Created": "2020-04-02T08:23:25.821236202Z",
        "Path": "/bin/bash",
        "Args": [],
        "State": {
            "Status": "running",
            "Running": true,
            "Paused": false,
            "Restarting": false,
            "OOMKilled": false,
            "Dead": false,
            "Pid": 27351,
            "ExitCode": 0,
            "Error": "",
            "StartedAt": "2020-04-02T08:23:26.832760863Z",
            "FinishedAt": "0001-01-01T00:00:00Z"
        },
        "Image": "sha256:4e5021d210f65ebe915670c7089120120bc0a303b90208592851708c1b8c04bd",
        "ResolvConfPath": "/var/lib/docker/containers/a21fc4f1b9afff09e6b08b79e923e5bccb06b81f735179762ee0be56c46337a6/resolv.conf",
        "HostnamePath": "/var/lib/docker/containers/a21fc4f1b9afff09e6b08b79e923e5bccb06b81f735179762ee0be56c46337a6/hostname",
        "HostsPath": "/var/lib/docker/containers/a21fc4f1b9afff09e6b08b79e923e5bccb06b81f735179762ee0be56c46337a6/hosts",
        "LogPath": "/var/lib/docker/containers/a21fc4f1b9afff09e6b08b79e923e5bccb06b81f735179762ee0be56c46337a6/a21fc4f1b9afff09e6b08b79e923e5bccb06b81f735179762ee0be56c46337a6-json.log",
        "Name": "/frosty_turing",
        "RestartCount": 0,
        "Driver": "overlay2",
        "Platform": "linux",
        "MountLabel": "",
        "ProcessLabel": "",
        "AppArmorProfile": "docker-default",
        "ExecIDs": null,
        "HostConfig": {
            "Binds": null,
            "ContainerIDFile": "",
            "LogConfig": {
                "Type": "json-file",
                "Config": {}
            },
            "NetworkMode": "default",
            "PortBindings": {},
            "RestartPolicy": {
                "Name": "no",
                "MaximumRetryCount": 0
            },
            "AutoRemove": false,
            "VolumeDriver": "",
            "VolumesFrom": null,
            "CapAdd": null,
            "CapDrop": null,
            "Dns": [],
            "DnsOptions": [],
            "DnsSearch": [],
            "ExtraHosts": null,
            "GroupAdd": null,
            "IpcMode": "shareable",
            "Cgroup": "",
            "Links": null,
            "OomScoreAdj": 0,
            "PidMode": "",
            "Privileged": false,
            "PublishAllPorts": false,
            "ReadonlyRootfs": false,
            "SecurityOpt": null,
            "UTSMode": "",
            "UsernsMode": "",
            "ShmSize": 67108864,
            "Runtime": "runc",
            "ConsoleSize": [
                0,
                0
            ],
            "Isolation": "",
            "CpuShares": 0,
            "Memory": 0,
            "NanoCpus": 0,
            "CgroupParent": "",
            "BlkioWeight": 0,
            "BlkioWeightDevice": [],
            "BlkioDeviceReadBps": null,
            "BlkioDeviceWriteBps": null,
            "BlkioDeviceReadIOps": null,
            "BlkioDeviceWriteIOps": null,
            "CpuPeriod": 0,
            "CpuQuota": 0,
            "CpuRealtimePeriod": 0,
            "CpuRealtimeRuntime": 0,
            "CpusetCpus": "",
            "CpusetMems": "",
            "Devices": [],
            "DeviceCgroupRules": null,
            "DiskQuota": 0,
            "KernelMemory": 0,
            "MemoryReservation": 0,
            "MemorySwap": 0,
            "MemorySwappiness": null,
            "OomKillDisable": false,
            "PidsLimit": 0,
            "Ulimits": null,
            "CpuCount": 0,
            "CpuPercent": 0,
            "IOMaximumIOps": 0,
            "IOMaximumBandwidth": 0,
            "MaskedPaths": [
                "/proc/asound",
                "/proc/acpi",
                "/proc/kcore",
                "/proc/keys",
                "/proc/latency_stats",
                "/proc/timer_list",
                "/proc/timer_stats",
                "/proc/sched_debug",
                "/proc/scsi",
                "/sys/firmware"
            ],
            "ReadonlyPaths": [
                "/proc/bus",
                "/proc/fs",
                "/proc/irq",
                "/proc/sys",
                "/proc/sysrq-trigger"
            ]
        },
        "GraphDriver": {
            "Data": {
                "LowerDir": "/var/lib/docker/overlay2/b1a87be9bd189d0161ede433fc5c68f1756689335bbb6934241a134eac63c36e-init/diff:/var/lib/docker/overlay2/922c2e4ac07a7907577b09099c9a4740177d7157ac025c8370e8924de3ecf887/diff:/var/lib/docker/overlay2/80299c38008196dc47319a3a7cf6ba3d83480e304c46d9d9665b2e560583c176/diff:/var/lib/docker/overlay2/dcd7138c66ae6db7456243de81e7c196d3ef9301bde12a8ec5d4346f04f03ece/diff:/var/lib/docker/overlay2/4dcbcdab2cf4c58012ea7849390830a05f6c033ed73f05cc239de400de869b36/diff",
                "MergedDir": "/var/lib/docker/overlay2/b1a87be9bd189d0161ede433fc5c68f1756689335bbb6934241a134eac63c36e/merged",
                "UpperDir": "/var/lib/docker/overlay2/b1a87be9bd189d0161ede433fc5c68f1756689335bbb6934241a134eac63c36e/diff",
                "WorkDir": "/var/lib/docker/overlay2/b1a87be9bd189d0161ede433fc5c68f1756689335bbb6934241a134eac63c36e/work"
            },
            "Name": "overlay2"
        },
        "Mounts": [],
        "Config": {
            "Hostname": "a21fc4f1b9af",
            "Domainname": "",
            "User": "",
            "AttachStdin": false,
            "AttachStdout": false,
            "AttachStderr": false,
            "Tty": true,
            "OpenStdin": true,
            "StdinOnce": false,
            "Env": [
                "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
            ],
            "Cmd": [
                "/bin/bash"
            ],
            "ArgsEscaped": true,
            "Image": "ubuntu",
            "Volumes": null,
            "WorkingDir": "",
            "Entrypoint": null,
            "OnBuild": null,
            "Labels": {}
        },
        "NetworkSettings": {
            "Bridge": "",
            "SandboxID": "f9486936abcfb88dc9b2ff014cf0e8d735df25028631ae8ea7c3b0f8e96d763f",
            "HairpinMode": false,
            "LinkLocalIPv6Address": "",
            "LinkLocalIPv6PrefixLen": 0,
            "Ports": {},
            "SandboxKey": "/var/run/docker/netns/f9486936abcf",
            "SecondaryIPAddresses": null,
            "SecondaryIPv6Addresses": null,
            "EndpointID": "e1dc0ef83c09b1086b4f647fcd47e6dcca84eba5e8b4c4aaf8f7c0f331ce7bb1",
            "Gateway": "172.17.0.1",
            "GlobalIPv6Address": "",
            "GlobalIPv6PrefixLen": 0,
            "IPAddress": "172.17.0.4",
            "IPPrefixLen": 16,
            "IPv6Gateway": "",
            "MacAddress": "02:42:ac:11:00:04",
            "Networks": {
                "bridge": {
                    "IPAMConfig": null,
                    "Links": null,
                    "Aliases": null,
                    "NetworkID": "be2cda653509c6435ec33c40e2b7e50d206af09c99cf7943ea7ef62a9e040888",
                    "EndpointID": "e1dc0ef83c09b1086b4f647fcd47e6dcca84eba5e8b4c4aaf8f7c0f331ce7bb1",
                    "Gateway": "172.17.0.1",
                    "IPAddress": "172.17.0.4",
                    "IPPrefixLen": 16,
                    "IPv6Gateway": "",
                    "GlobalIPv6Address": "",
                    "GlobalIPv6PrefixLen": 0,
                    "MacAddress": "02:42:ac:11:00:04",
                    "DriverOpts": null
                }
            }
        }
    }
]
~~~
___$ docker container run -dit --network bridge ubuntu___
~~~
dce10c70a387984bc0cbad909c93ca3a9c0d3c778af39adff9e3e521db38dc93
~~~
___$ docker network inspect bridge___
~~~
[
    {
        "Name": "bridge",
        "Id": "be2cda653509c6435ec33c40e2b7e50d206af09c99cf7943ea7ef62a9e040888",
        "Created": "2020-04-01T10:50:27.538671636-06:00",
        "Scope": "local",
        "Driver": "bridge",
        "EnableIPv6": false,
        "IPAM": {
            "Driver": "default",
            "Options": null,
            "Config": [
                {
                    "Subnet": "172.17.0.0/16",
                    "Gateway": "172.17.0.1"
                }
            ]
        },
        "Internal": false,
        "Attachable": false,
        "Ingress": false,
        "ConfigFrom": {
            "Network": ""
        },
        "ConfigOnly": false,
        "Containers": {
            "3f71be0b178b1a01bd74b6bb5a4bbe99a91c2c05d7d5b87ed6f94b10e4af7bcc": {
                "Name": "clever_morse",
                "EndpointID": "609e0a154407d76fb9a61074b9d857a3df0ff52f95f0e8b90afd367267e98768",
                "MacAddress": "02:42:ac:11:00:02",
                "IPv4Address": "172.17.0.2/16",
                "IPv6Address": ""
            },
            "a21fc4f1b9afff09e6b08b79e923e5bccb06b81f735179762ee0be56c46337a6": {
                "Name": "frosty_turing",
                "EndpointID": "e1dc0ef83c09b1086b4f647fcd47e6dcca84eba5e8b4c4aaf8f7c0f331ce7bb1",
                "MacAddress": "02:42:ac:11:00:04",
                "IPv4Address": "172.17.0.4/16",
                "IPv6Address": ""
            },
            "dce10c70a387984bc0cbad909c93ca3a9c0d3c778af39adff9e3e521db38dc93": {
                "Name": "compassionate_burnell",
                "EndpointID": "3d746bda8d20808df5ffbb34192d2aa3498dfc1ff217cc1db87c3131859f431d",
                "MacAddress": "02:42:ac:11:00:05",
                "IPv4Address": "172.17.0.5/16",
                "IPv6Address": ""
            },
            "f61611bc7ea37fe5f535afa927a1d849522223eadd66e0d76be0dc2954ee0cdb": {
                "Name": "keen_chatterjee",
                "EndpointID": "855dd42afa89f926ff769fcb886331eb985a145f1ae2b38ef830a58d07b28136",
                "MacAddress": "02:42:ac:11:00:03",
                "IPv4Address": "172.17.0.3/16",
                "IPv6Address": ""
            }
        },
        "Options": {
            "com.docker.network.bridge.default_bridge": "true",
            "com.docker.network.bridge.enable_icc": "true",
            "com.docker.network.bridge.enable_ip_masquerade": "true",
            "com.docker.network.bridge.host_binding_ipv4": "0.0.0.0",
            "com.docker.network.bridge.name": "docker0",
            "com.docker.network.driver.mtu": "1500"
        },
        "Labels": {}
    }
]
~~~
___$ docker container ls___
~~~
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                    NAMES
dce10c70a387        ubuntu              "/bin/bash"              52 seconds ago      Up 50 seconds                                compassionate_burnell
a21fc4f1b9af        ubuntu              "/bin/bash"              6 minutes ago       Up 6 minutes                                 frosty_turing
f61611bc7ea3        web                 "docker-entrypoint.s…"   36 minutes ago      Up 36 minutes       0.0.0.0:3000->3000/tcp   keen_chatterjee
3f71be0b178b        ubuntu-file:v9      "/bin/bash"              About an hour ago   Up About an hour                             clever_morse
~~~
___$ docker network disconnect bridge dce10c70a387___

___$ docker network inspect bridge___
~~~
[
    {
        "Name": "bridge",
        "Id": "be2cda653509c6435ec33c40e2b7e50d206af09c99cf7943ea7ef62a9e040888",
        "Created": "2020-04-01T10:50:27.538671636-06:00",
        "Scope": "local",
        "Driver": "bridge",
        "EnableIPv6": false,
        "IPAM": {
            "Driver": "default",
            "Options": null,
            "Config": [
                {
                    "Subnet": "172.17.0.0/16",
                    "Gateway": "172.17.0.1"
                }
            ]
        },
        "Internal": false,
        "Attachable": false,
        "Ingress": false,
        "ConfigFrom": {
            "Network": ""
        },
        "ConfigOnly": false,
        "Containers": {
            "3f71be0b178b1a01bd74b6bb5a4bbe99a91c2c05d7d5b87ed6f94b10e4af7bcc": {
                "Name": "clever_morse",
                "EndpointID": "609e0a154407d76fb9a61074b9d857a3df0ff52f95f0e8b90afd367267e98768",
                "MacAddress": "02:42:ac:11:00:02",
                "IPv4Address": "172.17.0.2/16",
                "IPv6Address": ""
            },
            "a21fc4f1b9afff09e6b08b79e923e5bccb06b81f735179762ee0be56c46337a6": {
                "Name": "frosty_turing",
                "EndpointID": "e1dc0ef83c09b1086b4f647fcd47e6dcca84eba5e8b4c4aaf8f7c0f331ce7bb1",
                "MacAddress": "02:42:ac:11:00:04",
                "IPv4Address": "172.17.0.4/16",
                "IPv6Address": ""
            },
            "f61611bc7ea37fe5f535afa927a1d849522223eadd66e0d76be0dc2954ee0cdb": {
                "Name": "keen_chatterjee",
                "EndpointID": "855dd42afa89f926ff769fcb886331eb985a145f1ae2b38ef830a58d07b28136",
                "MacAddress": "02:42:ac:11:00:03",
                "IPv4Address": "172.17.0.3/16",
                "IPv6Address": ""
            }
        },
        "Options": {
            "com.docker.network.bridge.default_bridge": "true",
            "com.docker.network.bridge.enable_icc": "true",
            "com.docker.network.bridge.enable_ip_masquerade": "true",
            "com.docker.network.bridge.host_binding_ipv4": "0.0.0.0",
            "com.docker.network.bridge.name": "docker0",
            "com.docker.network.driver.mtu": "1500"
        },
        "Labels": {}
    }
]
~~~
___$ docker network connect bridge dce10c70a387___

___$ docker network inspect bridge___
~~~
[
    {
        "Name": "bridge",
        "Id": "be2cda653509c6435ec33c40e2b7e50d206af09c99cf7943ea7ef62a9e040888",
        "Created": "2020-04-01T10:50:27.538671636-06:00",
        "Scope": "local",
        "Driver": "bridge",
        "EnableIPv6": false,
        "IPAM": {
            "Driver": "default",
            "Options": null,
            "Config": [
                {
                    "Subnet": "172.17.0.0/16",
                    "Gateway": "172.17.0.1"
                }
            ]
        },
        "Internal": false,
        "Attachable": false,
        "Ingress": false,
        "ConfigFrom": {
            "Network": ""
        },
        "ConfigOnly": false,
        "Containers": {
            "3f71be0b178b1a01bd74b6bb5a4bbe99a91c2c05d7d5b87ed6f94b10e4af7bcc": {
                "Name": "clever_morse",
                "EndpointID": "609e0a154407d76fb9a61074b9d857a3df0ff52f95f0e8b90afd367267e98768",
                "MacAddress": "02:42:ac:11:00:02",
                "IPv4Address": "172.17.0.2/16",
                "IPv6Address": ""
            },
            "a21fc4f1b9afff09e6b08b79e923e5bccb06b81f735179762ee0be56c46337a6": {
                "Name": "frosty_turing",
                "EndpointID": "e1dc0ef83c09b1086b4f647fcd47e6dcca84eba5e8b4c4aaf8f7c0f331ce7bb1",
                "MacAddress": "02:42:ac:11:00:04",
                "IPv4Address": "172.17.0.4/16",
                "IPv6Address": ""
            },
            "dce10c70a387984bc0cbad909c93ca3a9c0d3c778af39adff9e3e521db38dc93": {
                "Name": "compassionate_burnell",
                "EndpointID": "de8865b6eb644ac324059c413ee6c41ffbbf55acb1f079046e1a1e6e8e662a68",
                "MacAddress": "02:42:ac:11:00:05",
                "IPv4Address": "172.17.0.5/16",
                "IPv6Address": ""
            },
            "f61611bc7ea37fe5f535afa927a1d849522223eadd66e0d76be0dc2954ee0cdb": {
                "Name": "keen_chatterjee",
                "EndpointID": "855dd42afa89f926ff769fcb886331eb985a145f1ae2b38ef830a58d07b28136",
                "MacAddress": "02:42:ac:11:00:03",
                "IPv4Address": "172.17.0.3/16",
                "IPv6Address": ""
            }
        },
        "Options": {
            "com.docker.network.bridge.default_bridge": "true",
            "com.docker.network.bridge.enable_icc": "true",
            "com.docker.network.bridge.enable_ip_masquerade": "true",
            "com.docker.network.bridge.host_binding_ipv4": "0.0.0.0",
            "com.docker.network.bridge.name": "docker0",
            "com.docker.network.driver.mtu": "1500"
        },
        "Labels": {}
    }
]
~~~
___$ docker stop $(docker ps -q)___
~~~
dce10c70a387
a21fc4f1b9af
f61611bc7ea3
3f71be0b178b
~~~___$ docker container prune___
~~~
WARNING! This will remove all stopped containers.
Are you sure you want to continue? [y/N] y
Deleted Containers:
dce10c70a387984bc0cbad909c93ca3a9c0d3c778af39adff9e3e521db38dc93
a21fc4f1b9afff09e6b08b79e923e5bccb06b81f735179762ee0be56c46337a6
f61611bc7ea37fe5f535afa927a1d849522223eadd66e0d76be0dc2954ee0cdb
320d507d6fc3edf231c71249674bf3bc77c893bb44f12bbeeab12b67b8cd92c6
5b66bd31b032ab4678681bd11f01149a054d6a1b504f17dcb0a34553e8e86ab3
74e7945177642f28d2334d011cc329688adffb788acb8cfbae539ebd4d291c34
30eb16ec90e320cca871288e880cd0068379683db4b7c93df5a13f58782a8026
a1661370ae7667389f928b0bcf951bd5d5b0c95b94b3a37e0f63185a74d0c98f
0bcb64e06f2bd99455dcbadcad99a0a75071190bb608754936b6502d3d8d00f9
7613bbe372ca936beb628a3597064d74f6f92e731c0c4184350a3d631ee32580
0795d8526f1a4d253a6613354adc11bd2422bfa2fdf08a70d7564083a219aad9
9f6be9a7ee01ef0c00472f6601bee86d79a39d3b4e739d6516bdf72b8a1db506
3f71be0b178b1a01bd74b6bb5a4bbe99a91c2c05d7d5b87ed6f94b10e4af7bcc
089495cf34e99bb1168c451aee342b5310ffd1cdc8a0381c1e6b7a0cb683c944
986cf4854f6cd1500f8db306454444d5877dea88bb61aa65fa73b4aa74a9538c

Total reclaimed space: 147B
~~~
___$ docker container run -dit --network none ubuntu___
~~~
002946b714efb3ebc893e9b8abea84bc4e18357ad2c1ea92eac89893e65006a8
~~~
___$ docker container run -dit --network host ubuntu___
~~~
2c31a6676a3d5039cc93ff5130305ea1193139151d811d7cddad4bc3f4c2a177
~~~
___$ docker container ls___
~~~
 CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
2c31a6676a3d        ubuntu              "/bin/bash"         27 seconds ago      Up 25 seconds                           infallible_banzai
002946b714ef        ubuntu              "/bin/bash"         14 minutes ago      Up 14 minutes                           unruffled_poincare                          unruffled_poincare
~~~
___$ docker network inspect none___
~~~
[
    {
        "Name": "none",
        "Id": "02234d7f44def1e564a9c3ac8180e179ad6ac02967fcd79850e315dadfe8c368",
        "Created": "2020-02-27T01:16:48.40229387-06:00",
        "Scope": "local",
        "Driver": "null",
        "EnableIPv6": false,
        "IPAM": {
            "Driver": "default",
            "Options": null,
            "Config": []
        },
        "Internal": false,
        "Attachable": false,
        "Ingress": false,
        "ConfigFrom": {
            "Network": ""
        },
        "ConfigOnly": false,
        "Containers": {
            "002946b714efb3ebc893e9b8abea84bc4e18357ad2c1ea92eac89893e65006a8": {
                "Name": "unruffled_poincare",
                "EndpointID": "c60cf556c7a93787c3fa72d8a6c9e93789a81c3b52b8b0c8c7f769d5e46171a9",
                "MacAddress": "",
                "IPv4Address": "",
                "IPv6Address": ""
            }
        },
        "Options": {},
        "Labels": {}
    }
]
~~~
___$ docker container exec -it 002946b714ef bash___
~~~
root@002946b714ef:/# apt update
Err:1 http://archive.ubuntu.com/ubuntu bionic InRelease
  Temporary failure resolving 'archive.ubuntu.com'
Err:2 http://security.ubuntu.com/ubuntu bionic-security InRelease
  Temporary failure resolving 'security.ubuntu.com'
Err:3 http://archive.ubuntu.com/ubuntu bionic-updates InRelease
  Temporary failure resolving 'archive.ubuntu.com'
Err:4 http://archive.ubuntu.com/ubuntu bionic-backports InRelease
  Temporary failure resolving 'archive.ubuntu.com'
Reading package lists... Done
Building dependency tree
Reading state information... Done
All packages are up to date.
W: Failed to fetch http://archive.ubuntu.com/ubuntu/dists/bionic/InRelease  Temporary failure resolving 'archive.ubuntu.com'
W: Failed to fetch http://archive.ubuntu.com/ubuntu/dists/bionic-updates/InRelease  Temporary failure resolving 'archive.ubuntu.com'
W: Failed to fetch http://archive.ubuntu.com/ubuntu/dists/bionic-backports/InRelease  Temporary failure resolving 'archive.ubuntu.com'
W: Failed to fetch http://security.ubuntu.com/ubuntu/dists/bionic-security/InRelease  Temporary failure resolving 'security.ubuntu.com'
W: Some index files failed to download. They have been ignored, or old ones used instead.
~~~
Cualquier operación de red debe fallar, por ejemplo un "apt-get".

___$ docker network inspect host___
~~~
[
    {
        "Name": "host",
        "Id": "ef2473a96188ec5b074a4d072ab83126ab1657c9c639d637e81674d61448dcb6",
        "Created": "2020-02-27T01:16:48.538989456-06:00",
        "Scope": "local",
        "Driver": "host",
        "EnableIPv6": false,
        "IPAM": {
            "Driver": "default",
            "Options": null,
            "Config": []
        },
        "Internal": false,
        "Attachable": false,
        "Ingress": false,
        "ConfigFrom": {
            "Network": ""
        },
        "ConfigOnly": false,
        "Containers": {
            "2c31a6676a3d5039cc93ff5130305ea1193139151d811d7cddad4bc3f4c2a177": {
                "Name": "infallible_banzai",
                "EndpointID": "eea9d0fcbb65b6ec256582a36b0b3e6ffec6b498b072499c814fba5241458386",
                "MacAddress": "",
                "IPv4Address": "",
                "IPv6Address": ""
            }
        },
        "Options": {},
        "Labels": {}
    }
]
~~~
___$ docker container exec -it 2c31a6676a3d bash___
~~~
root@bonampak:/# apt update
Get:1 http://security.ubuntu.com/ubuntu bionic-security InRelease [88.7 kB]
Get:2 http://archive.ubuntu.com/ubuntu bionic InRelease [242 kB]
Get:3 http://security.ubuntu.com/ubuntu bionic-security/universe amd64 Packages [835 kB]
Get:4 http://archive.ubuntu.com/ubuntu bionic-updates InRelease [88.7 kB]
Get:5 http://security.ubuntu.com/ubuntu bionic-security/multiverse amd64 Packages [7904 B]
Get:6 http://security.ubuntu.com/ubuntu bionic-security/main amd64 Packages [870 kB]
Get:7 http://archive.ubuntu.com/ubuntu bionic-backports InRelease [74.6 kB]
Get:8 http://archive.ubuntu.com/ubuntu bionic/main amd64 Packages [1344 kB]
Get:9 http://security.ubuntu.com/ubuntu bionic-security/restricted amd64 Packages [37.0 kB]
Get:10 http://archive.ubuntu.com/ubuntu bionic/multiverse amd64 Packages [186 kB]
Get:11 http://archive.ubuntu.com/ubuntu bionic/restricted amd64 Packages [13.5 kB]
Get:12 http://archive.ubuntu.com/ubuntu bionic/universe amd64 Packages [11.3 MB]
Get:13 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 Packages [1161 kB]
Get:14 http://archive.ubuntu.com/ubuntu bionic-updates/universe amd64 Packages [1367 kB]
Get:15 http://archive.ubuntu.com/ubuntu bionic-updates/multiverse amd64 Packages [12.2 kB]
Get:16 http://archive.ubuntu.com/ubuntu bionic-updates/restricted amd64 Packages [50.4 kB]
Get:17 http://archive.ubuntu.com/ubuntu bionic-backports/universe amd64 Packages [4247 B]
Get:18 http://archive.ubuntu.com/ubuntu bionic-backports/main amd64 Packages [2496 B]
Fetched 17.7 MB in 17s (1028 kB/s)
Reading package lists... Done
Building dependency tree
Reading state information... Done
12 packages can be upgraded. Run 'apt list --upgradable' to see them.

root@bonampak:/# apt install net-tools
Reading package lists... Done
Building dependency tree
Reading state information... Done
The following NEW packages will be installed:
  net-tools
0 upgraded, 1 newly installed, 0 to remove and 12 not upgraded.
Need to get 194 kB of archives.
After this operation, 803 kB of additional disk space will be used.
Get:1 http://archive.ubuntu.com/ubuntu bionic/main amd64 net-tools amd64 1.60+git20161116.90da8a0-1ubuntu1 [194 kB]
Fetched 194 kB in 1s (174 kB/s)
debconf: delaying package configuration, since apt-utils is not installed
Selecting previously unselected package net-tools.
(Reading database ... 4046 files and directories currently installed.)
Preparing to unpack .../net-tools_1.60+git20161116.90da8a0-1ubuntu1_amd64.deb ...
Unpacking net-tools (1.60+git20161116.90da8a0-1ubuntu1) ...
Setting up net-tools (1.60+git20161116.90da8a0-1ubuntu1) ...

root@bonampak:/# ifconfig -a
docker0: flags=4099<UP,BROADCAST,MULTICAST>  mtu 1500
        inet 172.17.0.1  netmask 255.255.0.0  broadcast 172.17.255.255
        ether 02:42:44:f8:90:93  txqueuelen 0  (Ethernet)
        RX packets 1812  bytes 189884 (189.8 KB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 2641  bytes 2928868 (2.9 MB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

eth0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.1.71  netmask 255.255.255.0  broadcast 192.168.1.255
        ether 00:1e:8c:f2:0c:0d  txqueuelen 1000  (Ethernet)
        RX packets 2538074  bytes 2330103599 (2.3 GB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 1338786  bytes 486515946 (486.5 MB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
        device interrupt 20  memory 0xfe700000-fe720000

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        loop  txqueuelen 1000  (Local Loopback)
        RX packets 116635  bytes 52120419 (52.1 MB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 116635  bytes 52120419 (52.1 MB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

virbr0: flags=4099<UP,BROADCAST,MULTICAST>  mtu 1500
        inet 192.168.122.1  netmask 255.255.255.0  broadcast 192.168.122.255
        ether 52:54:00:cd:a5:59  txqueuelen 1000  (Ethernet)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

virbr0-nic: flags=4098<BROADCAST,MULTICAST>  mtu 1500
        ether 52:54:00:cd:a5:59  txqueuelen 1000  (Ethernet)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
~~~

Dentro del contenedor ejecutar el comando "ifconfig -a". Deben mostrarse dos interfaces de red, la Docker0 y una ETH0 con la misma dirección IP del Hosts.
~~~
### Creando Redes
___$ vi Dockerfile___

FROM ubuntu

RUN apt-get update && apt-get install -y net-tools iputils-ping

___$ docker image build -t ubuntu-tools .___
~~~
Sending build context to Docker daemon  7.215GB
Step 1/2 : FROM ubuntu
 ---> 4e5021d210f6
Step 2/2 : RUN apt-get update && apt-get install -y net-tools iputils-ping
 ---> Running in b8a7781deeda
Get:1 http://security.ubuntu.com/ubuntu bionic-security InRelease [88.7 kB]
Get:2 http://archive.ubuntu.com/ubuntu bionic InRelease [242 kB]
Get:3 http://security.ubuntu.com/ubuntu bionic-security/multiverse amd64 Packages [7904 B]
Get:4 http://security.ubuntu.com/ubuntu bionic-security/universe amd64 Packages [835 kB]
Get:5 http://archive.ubuntu.com/ubuntu bionic-updates InRelease [88.7 kB]
Get:6 http://security.ubuntu.com/ubuntu bionic-security/restricted amd64 Packages [37.0 kB]
Get:7 http://security.ubuntu.com/ubuntu bionic-security/main amd64 Packages [870 kB]
Get:8 http://archive.ubuntu.com/ubuntu bionic-backports InRelease [74.6 kB]
Get:9 http://archive.ubuntu.com/ubuntu bionic/universe amd64 Packages [11.3 MB]
Get:10 http://archive.ubuntu.com/ubuntu bionic/restricted amd64 Packages [13.5 kB]
Get:11 http://archive.ubuntu.com/ubuntu bionic/multiverse amd64 Packages [186 kB]
Get:12 http://archive.ubuntu.com/ubuntu bionic/main amd64 Packages [1344 kB]
Get:13 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 Packages [1161 kB]
Get:14 http://archive.ubuntu.com/ubuntu bionic-updates/multiverse amd64 Packages [12.2 kB]
Get:15 http://archive.ubuntu.com/ubuntu bionic-updates/universe amd64 Packages [1367 kB]
Get:16 http://archive.ubuntu.com/ubuntu bionic-updates/restricted amd64 Packages [50.4 kB]
Get:17 http://archive.ubuntu.com/ubuntu bionic-backports/main amd64 Packages [2496 B]
Get:18 http://archive.ubuntu.com/ubuntu bionic-backports/universe amd64 Packages [4247 B]
Fetched 17.7 MB in 17s (1056 kB/s)
Reading package lists...
Reading package lists...
Building dependency tree...
Reading state information...
The following additional packages will be installed:
  libcap2 libcap2-bin libidn11 libpam-cap
The following NEW packages will be installed:
  iputils-ping libcap2 libcap2-bin libidn11 libpam-cap net-tools
0 upgraded, 6 newly installed, 0 to remove and 12 not upgraded.
Need to get 336 kB of archives.
After this operation, 1339 kB of additional disk space will be used.
Get:1 http://archive.ubuntu.com/ubuntu bionic/main amd64 libcap2 amd64 1:2.25-1.2 [13.0 kB]
Get:2 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 libidn11 amd64 1.33-2.1ubuntu1.2 [46.6 kB]
Get:3 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 iputils-ping amd64 3:20161105-1ubuntu3 [54.2 kB]
Get:4 http://archive.ubuntu.com/ubuntu bionic/main amd64 libcap2-bin amd64 1:2.25-1.2 [20.6 kB]
Get:5 http://archive.ubuntu.com/ubuntu bionic/main amd64 libpam-cap amd64 1:2.25-1.2 [7268 B]
Get:6 http://archive.ubuntu.com/ubuntu bionic/main amd64 net-tools amd64 1.60+git20161116.90da8a0-1ubuntu1 [194 kB]
debconf: delaying package configuration, since apt-utils is not installed
Fetched 336 kB in 1s (250 kB/s)
Selecting previously unselected package libcap2:amd64.
(Reading database ... 4046 files and directories currently installed.)
Preparing to unpack .../0-libcap2_1%3a2.25-1.2_amd64.deb ...
Unpacking libcap2:amd64 (1:2.25-1.2) ...
Selecting previously unselected package libidn11:amd64.
Preparing to unpack .../1-libidn11_1.33-2.1ubuntu1.2_amd64.deb ...
Unpacking libidn11:amd64 (1.33-2.1ubuntu1.2) ...
Selecting previously unselected package iputils-ping.
Preparing to unpack .../2-iputils-ping_3%3a20161105-1ubuntu3_amd64.deb ...
Unpacking iputils-ping (3:20161105-1ubuntu3) ...
Selecting previously unselected package libcap2-bin.
Preparing to unpack .../3-libcap2-bin_1%3a2.25-1.2_amd64.deb ...
Unpacking libcap2-bin (1:2.25-1.2) ...
Selecting previously unselected package libpam-cap:amd64.
Preparing to unpack .../4-libpam-cap_1%3a2.25-1.2_amd64.deb ...
Unpacking libpam-cap:amd64 (1:2.25-1.2) ...
Selecting previously unselected package net-tools.
Preparing to unpack .../5-net-tools_1.60+git20161116.90da8a0-1ubuntu1_amd64.deb ...
Unpacking net-tools (1.60+git20161116.90da8a0-1ubuntu1) ...
Setting up libcap2:amd64 (1:2.25-1.2) ...
Setting up net-tools (1.60+git20161116.90da8a0-1ubuntu1) ...
Setting up libidn11:amd64 (1.33-2.1ubuntu1.2) ...
Setting up iputils-ping (3:20161105-1ubuntu3) ...
Setting up libpam-cap:amd64 (1:2.25-1.2) ...
debconf: unable to initialize frontend: Dialog
debconf: (TERM is not set, so the dialog frontend is not usable.)
debconf: falling back to frontend: Readline
debconf: unable to initialize frontend: Readline
debconf: (Can't locate Term/ReadLine.pm in @INC (you may need to install the Term::ReadLine module) (@INC contains: /etc/perl /usr/local/lib/x86_64-linux-gnu/perl/5.26.1 /usr/local/share/perl/5.26.1 /usr/lib/x86_64-linux-gnu/perl5/5.26 /usr/share/perl5 /usr/lib/x86_64-linux-gnu/perl/5.26 /usr/share/perl/5.26 /usr/local/lib/site_perl /usr/lib/x86_64-linux-gnu/perl-base) at /usr/share/perl5/Debconf/FrontEnd/Readline.pm line 7.)
debconf: falling back to frontend: Teletype
Setting up libcap2-bin (1:2.25-1.2) ...
Processing triggers for libc-bin (2.27-3ubuntu1) ...
Removing intermediate container b8a7781deeda
 ---> 4f1b588af01a
Successfully built 4f1b588af01a
Successfully tagged ubuntu-tools:latest
~~~
___$ docker network create --help___
~~~

~~~
___$ docker network create br0___
~~~
21053de24f36c5190f7ad5557dc66238c0cd683fe1c8c7088c35a438bcb3bb80
~~~
___$ docker network ls___
~~~
NETWORK ID          NAME                DRIVER              SCOPE
21053de24f36        br0                 bridge              local
be2cda653509        bridge              bridge              local
ef2473a96188        host                host                local
02234d7f44de        none                null                local
~~~
___$ docker network inspect br0___
~~~
[
    {
        "Name": "br0",
        "Id": "21053de24f36c5190f7ad5557dc66238c0cd683fe1c8c7088c35a438bcb3bb80",
        "Created": "2020-04-02T02:59:48.549885793-06:00",
        "Scope": "local",
        "Driver": "bridge",
        "EnableIPv6": false,
        "IPAM": {
            "Driver": "default",
            "Options": {},
            "Config": [
                {
                    "Subnet": "172.18.0.0/16",
                    "Gateway": "172.18.0.1"
                }
            ]
        },
        "Internal": false,
        "Attachable": false,
        "Ingress": false,
        "ConfigFrom": {
            "Network": ""
        },
        "ConfigOnly": false,
        "Containers": {},
        "Options": {},
        "Labels": {}
    }
]
~~~
___$ docker container run -dit --network br0 --name hosts1 ubuntu-tools___
~~~
ff3173d61983ed394f6c936fba89b8c116478056727666c7ca8cd26b601aede4
~~~
___$ docker container run -dit --network br0 --name hosts2 ubuntu-tools___
~~~
109250a3051f0c3f2ebeb8725caf8628e131ca15e2a8eaf1dd336f925e84c9ab
~~~
___$ docker container ls___
~~~
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
109250a3051f        ubuntu-tools        "/bin/bash"         10 seconds ago      Up 8 seconds                            hosts2
ff3173d61983        ubuntu-tools        "/bin/bash"         25 seconds ago      Up 23 seconds                           hosts1
2c31a6676a3d        ubuntu              "/bin/bash"         13 minutes ago      Up 13 minutes                           infallible_banzai
002946b714ef        ubuntu              "/bin/bash"         27 minutes ago      Up 27 minutes                           unruffled_poincare
~~~
___$ docker container exec -it hosts1 bash___
~~~
root@ff3173d61983:/# ifconfig -a
eth0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 172.18.0.2  netmask 255.255.0.0  broadcast 172.18.255.255
        ether 02:42:ac:12:00:02  txqueuelen 0  (Ethernet)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        loop  txqueuelen 1000  (Local Loopback)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

root@ff3173d61983:/# ping -c 3 172.18.0.3
PING 172.18.0.3 (172.18.0.3) 56(84) bytes of data.
64 bytes from 172.18.0.3: icmp_seq=1 ttl=64 time=0.154 ms
64 bytes from 172.18.0.3: icmp_seq=2 ttl=64 time=0.091 ms
64 bytes from 172.18.0.3: icmp_seq=3 ttl=64 time=0.088 ms

--- 172.18.0.3 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2039ms
rtt min/avg/max/mdev = 0.088/0.111/0.154/0.030 ms
~~~
___$ docker container exec -it hosts2 bash___
~~~
eth0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 172.18.0.3  netmask 255.255.0.0  broadcast 172.18.255.255
        ether 02:42:ac:12:00:03  txqueuelen 0  (Ethernet)
        RX packets 1  bytes 183 (183.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        loop  txqueuelen 1000  (Local Loopback)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

root@109250a3051f:/# ping -c 3 172.18.0.3
PING 172.18.0.3 (172.18.0.3) 56(84) bytes of data.
64 bytes from 172.18.0.3: icmp_seq=1 ttl=64 time=0.067 ms
64 bytes from 172.18.0.3: icmp_seq=2 ttl=64 time=0.053 ms
64 bytes from 172.18.0.3: icmp_seq=3 ttl=64 time=0.053 ms

--- 172.18.0.3 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2040ms
rtt min/avg/max/mdev = 0.053/0.057/0.067/0.011 ms
~~~
Dentro de los contenedores ejecutar "ifconfig -a"

Lanzar pings entre contenedores por dirección ip y por nombre de hosts.

### Agregando IP estática a contenedor
___$ docker network create --help___
~~~
Usage:	docker network create [OPTIONS] NETWORK

Create a network

Options:
      --attachable           Enable manual container attachment
      --aux-address map      Auxiliary IPv4 or IPv6 addresses used by Network driver (default map[])
      --config-from string   The network from which copying the configuration
      --config-only          Create a configuration only network
  -d, --driver string        Driver to manage the Network (default "bridge")
      --gateway strings      IPv4 or IPv6 Gateway for the master subnet
      --ingress              Create swarm routing-mesh network
      --internal             Restrict external access to the network
      --ip-range strings     Allocate container ip from a sub-range
      --ipam-driver string   IP Address Management Driver (default "default")
      --ipam-opt map         Set IPAM driver specific options (default map[])
      --ipv6                 Enable IPv6 networking
      --label list           Set metadata on a network
  -o, --opt map              Set driver specific options (default map[])
      --scope string         Control the network's scope
      --subnet strings       Subnet in CIDR format that represents a network segment
~~~
___$ docker network create --subnet 10.1.0.0/24 --gateway 10.1.0.1 br02___
~~~
62ceb8ec2e8d6178d752285db2ad009a17481c5adb4460a6a74f411dbaa80e79
~~~
___$ docker network ls___
~~~
NETWORK ID          NAME                DRIVER              SCOPE
21053de24f36        br0                 bridge              local
62ceb8ec2e8d        br02                bridge              local
be2cda653509        bridge              bridge              local
ef2473a96188        host                host                local
02234d7f44de        none                null                local
~~~
___$ docker container run -it --ip 10.1.0.10 --network br02 ubuntu-tools___
~~~
root@900fe333df39:/# ifconfig -a
eth0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 10.1.0.10  netmask 255.255.255.0  broadcast 10.1.0.255
        ether 02:42:0a:01:00:0a  txqueuelen 0  (Ethernet)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        loop  txqueuelen 1000  (Local Loopback)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
~~~
Dentro del contenedor ejecutar "ifconfig -a" para validar la ip que se le dio.

En otra terminal revisar la red br02.

___$ docker network inspect br02___
~~~
[
    {
        "Name": "br02",
        "Id": "62ceb8ec2e8d6178d752285db2ad009a17481c5adb4460a6a74f411dbaa80e79",
        "Created": "2020-04-02T03:05:44.570930512-06:00",
        "Scope": "local",
        "Driver": "bridge",
        "EnableIPv6": false,
        "IPAM": {
            "Driver": "default",
            "Options": {},
            "Config": [
                {
                    "Subnet": "10.1.0.0/24",
                    "Gateway": "10.1.0.1"
                }
            ]
        },
        "Internal": false,
        "Attachable": false,
        "Ingress": false,
        "ConfigFrom": {
            "Network": ""
        },
        "ConfigOnly": false,
        "Containers": {
            "eddb64577b31a432b3475896ce95254bf4be9614b60ebdce4844e36ff0a212a0": {
                "Name": "eloquent_panini",
                "EndpointID": "60b42eb7b3b947a19a40bbd48cf4923e1360a35f0d64092a79e4598da7811b43",
                "MacAddress": "02:42:0a:01:00:0a",
                "IPv4Address": "10.1.0.10/24",
                "IPv6Address": ""
            }
        },
        "Options": {},
        "Labels": {}
    }
]
~~~
## 6. Docker Machine
***
### Overview Docker Machine
https://docs.docker.com/machine/install-machine/
https://docs.docker.com/machine/get-started/
~~~
$ base=https://github.com/docker/machine/releases/download/v0.16.2 &&
  curl -L $base/docker-machine-$(uname -s)-$(uname -m) >/tmp/docker-machine &&
  sudo mv /tmp/docker-machine /usr/local/bin/docker-machine &&
  chmod +x /usr/local/bin/docker-machine

  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   617  100   617    0     0    705      0 --:--:-- --:--:-- --:--:--   705
100 26.8M  100 26.8M    0     0   986k      0  0:00:27  0:00:27 --:--:-- 1085k
~~~
___$ docker-machine --version___
~~~
docker-machine version 0.16.2, build bd45ab13
~~~
### Creando máquina DigitalOcean y VirtualBox
El API Token se genera desde DigitalOcean.

___$ docker-machine create -d digitalocean --digitalocean-access-token=jhasd87asd87asdf87sdf___

___$ docker-machine ls___

__Los pasos anteriores solo son posible si se tienen una cuenta de DigitalOcean__.

### Para instalar Virtualbox en Debian Buster

Para crear una con VirtualBox, primero debe tenerse instalado VirtualBox.
~~~
$ wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | sudo apt-key add -

$ wget -q https://www.virtualbox.org/download/oracle_vbox.asc -O- | sudo apt-key add -

$ cat /etc/apt/sources.list

# VirtualBox
deb http://download.virtualbox.org/virtualbox/debian buster contrib

$ apt install virtualbox-6.1

$ sudo usermod -aG vboxusers gvillafu

$ newgrp vboxusers gvillafu

$ id
uid=1000(gvillafu) gid=130(vboxusers) groups=130(vboxusers),24(cdrom),25(floppy),29(audio),30(dip),44(video),46(plugdev),101(systemd-journal),109(netdev),126(docker),127(libvirt),128(bluetooth),1000(gvillafu)
~~~
___$ docker-machine create --driver=virtualbox demo___
~~~
Running pre-create checks...
Creating machine...
(demo) Copying /home/gvillafu/.docker/machine/cache/boot2docker.iso to /home/gvillafu/.docker/machine/machines/demo/boot2docker.iso...
(demo) Creating VirtualBox VM...
(demo) Creating SSH key...
(demo) Starting the VM...
(demo) Check network to re-create if needed...
(demo) Found a new host-only adapter: "vboxnet0"
(demo) Waiting for an IP...
Waiting for machine to be running, this may take a few minutes...
Detecting operating system of created instance...
Waiting for SSH to be available...
Detecting the provisioner...
Provisioning with boot2docker...
Copying certs to the local machine directory...
Copying certs to the remote machine...
Setting Docker configuration on the remote daemon...
Checking connection to Docker...
Docker is up and running!
To see how to connect your Docker Client to the Docker Engine running on this virtual machine, run: docker-machine env demo
~~~
___$ docker-machine ls___
~~~
NAME   ACTIVE   DRIVER       STATE     URL                         SWARM   DOCKER     ERRORS
demo   -        virtualbox   Running   tcp://192.168.99.100:2376           v19.03.5
~~~
### Accediendo al docker machine
___$ docker-machine___
~~~
Usage: docker-machine [OPTIONS] COMMAND [arg...]

Create and manage machines running Docker.

Version: 0.16.2, build bd45ab13

Author:
  Docker Machine Contributors - <https://github.com/docker/machine>

Options:
  --debug, -D						Enable debug mode
  --storage-path, -s "/home/gvillafu/.docker/machine"	Configures storage path [$MACHINE_STORAGE_PATH]
  --tls-ca-cert 					CA to verify remotes against [$MACHINE_TLS_CA_CERT]
  --tls-ca-key 						Private key to generate certificates [$MACHINE_TLS_CA_KEY]
  --tls-client-cert 					Client cert to use for TLS [$MACHINE_TLS_CLIENT_CERT]
  --tls-client-key 					Private key used in client TLS auth [$MACHINE_TLS_CLIENT_KEY]
  --github-api-token 					Token to use for requests to the Github API [$MACHINE_GITHUB_API_TOKEN]
  --native-ssh						Use the native (Go-based) SSH implementation. [$MACHINE_NATIVE_SSH]
  --bugsnag-api-token 					BugSnag API token for crash reporting [$MACHINE_BUGSNAG_API_TOKEN]
  --help, -h						show help
  --version, -v						print the version

Commands:
  active		Print which machine is active
  config		Print the connection config for machine
  create		Create a machine
  env			Display the commands to set up the environment for the Docker client
  inspect		Inspect information about a machine
  ip			Get the IP address of a machine
  kill			Kill a machine
  ls			List machines
  provision		Re-provision existing machines
  regenerate-certs	Regenerate TLS Certificates for a machine
  restart		Restart a machine
  rm			Remove a machine
  ssh			Log into or run a command on a machine with SSH.
  scp			Copy files between machines
  mount			Mount or unmount a directory from a machine with SSHFS.
  start			Start a machine
  status		Get the status of a machine
  stop			Stop a machine
  upgrade		Upgrade a machine to the latest version of Docker
  url			Get the URL of a machine
  version		Show the Docker Machine version or a machine docker version
  help			Shows a list of commands or help for one command

Run 'docker-machine COMMAND --help' for more information on a command.
~~~
___$ docker-machine env demo___
~~~
export DOCKER_TLS_VERIFY="1"
export DOCKER_HOST="tcp://192.168.99.100:2376"
export DOCKER_CERT_PATH="/home/gvillafu/.docker/machine/machines/demo"
export DOCKER_MACHINE_NAME="demo"
'# Run this command to configure your shell:'
'# eval $(docker-machine env demo)'
~~~
La última línea debe decir "eval $(docker-machine env vbox-test)".

Donde dice ACTIVE debe aparecer un guión "-".

___$ eval $(docker-machine env demo)___

___$ docker-machine ls___
~~~
NAME   ACTIVE   DRIVER       STATE     URL                         SWARM   DOCKER     ERRORS
demo   *        virtualbox   Running   tcp://192.168.99.100:2376           v19.03.5
~~~
Donde dice ACTIVE debe aparecer un guión "*". Esto quiere decir que nuestro cliente esta conectado al docker daemon de nuestra maquina virtual.

A partir de ahora y utilizando el cliente docker, estaremos trabajando en el docker engine de vbox-test.

___$ docker image ls___
~~~
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
~~~
___$ docker image pull nginx___
~~~
Using default tag: latest
latest: Pulling from library/nginx
c499e6d256d6: Downloading [================>                                  ]  8.952MB/27.09MB
74cda408e262: Downloading [=====================>                             ]  10.11MB/23.92MBc499e6d256d6: Pull complete
74cda408e262: Pull complete
ffadbd415ab7: Pull complete
Digest: sha256:282530fcb7cd19f3848c7b611043f82ae4be3781cb00105a1d593d7e6286b596
Status: Downloaded newer image for nginx:latest
docker.io/library/nginx:latest
~~~
___$ docker container run -d -p 80:80 nginx___
~~~
34e5a23627a1e9fec9c47ec3d0f1b7987b1887a5d5810d3cb04f36d46c9eb802
~~~
___$ docker-machine ip demo___
~~~
192.168.99.100

curl 192.168.99.100
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>
~~~
Desde la ip que se muestra entrar por el navegador y debe verse la página de inicio de nginx.

___$ docker-machine ssh demo___
~~~
   ( '>')
  /) TC (\   Core is distributed with ABSOLUTELY NO WARRANTY.
 (/-_--_-\)           www.tinycorelinux.net

docker@demo:~$
~~~
___$ docker ps___
~~~
CONTAINER ID        IMAGE               COMMAND                  CREATED              STATUS              PORTS                NAMES
34e5a23627a1        nginx               "nginx -g 'daemon of…"   About a minute ago   Up About a minute   0.0.0.0:80->80/tcp   cranky_haibt

docker@demo:~$ exit
logout
~~~
___$ docker-machine rm demo___
~~~
About to remove demo
WARNING: This action will delete both local reference and remote instance.
Are you sure? (y/n): y
Successfully removed demo
~~~

# Fallo el capitulo de variables y docker-machine

## 7. Docker Compose
***
### Docker compose overview
https://docs.docker.com/compose/install/

___$ sudo curl -L "https://github.com/docker/compose/releases/download/1.25.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose___
~~~
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   617  100   617    0     0   1569      0 --:--:-- --:--:-- --:--:--  1565
100 16.3M  100 16.3M    0     0  1107k      0  0:00:15  0:00:15 --:--:-- 1189k
~~~
___$ sudo chmod +x /usr/local/bin/docker-compose___

___$ docker-compose --version___
~~~
docker-compose version 1.25.4, build 8d51620a
~~~
___$ docker-compose___
~~~
Define and run multi-container applications with Docker.

Usage:
  docker-compose [-f <arg>...] [options] [COMMAND] [ARGS...]
  docker-compose -h|--help

Options:
  -f, --file FILE             Specify an alternate compose file
                              (default: docker-compose.yml)
  -p, --project-name NAME     Specify an alternate project name
                              (default: directory name)
  --verbose                   Show more output
  --log-level LEVEL           Set log level (DEBUG, INFO, WARNING, ERROR, CRITICAL)
  --no-ansi                   Do not print ANSI control characters
  -v, --version               Print version and exit
  -H, --host HOST             Daemon socket to connect to

  --tls                       Use TLS; implied by --tlsverify
  --tlscacert CA_PATH         Trust certs signed only by this CA
  --tlscert CLIENT_CERT_PATH  Path to TLS certificate file
  --tlskey TLS_KEY_PATH       Path to TLS key file
  --tlsverify                 Use TLS and verify the remote
  --skip-hostname-check       Don't check the daemon's hostname against the
                              name specified in the client certificate
  --project-directory PATH    Specify an alternate working directory
                              (default: the path of the Compose file)
  --compatibility             If set, Compose will attempt to convert keys
                              in v3 files to their non-Swarm equivalent
  --env-file PATH             Specify an alternate environment file

Commands:
  build              Build or rebuild services
  config             Validate and view the Compose file
  create             Create services
  down               Stop and remove containers, networks, images, and volumes
  events             Receive real time events from containers
  exec               Execute a command in a running container
  help               Get help on a command
  images             List images
  kill               Kill containers
  logs               View output from containers
  pause              Pause services
  port               Print the public port for a port binding
  ps                 List containers
  pull               Pull service images
  push               Push service images
  restart            Restart services
  rm                 Remove stopped containers
  run                Run a one-off command
  scale              Set number of containers for a service
  start              Start services
  stop               Stop services
  top                Display the running processes
  unpause            Unpause services
  up                 Create and start containers
  version            Show the Docker-Compose version information
~~~
### Servicios
___$ vi docker-compose.yml___
~~~
version: "3.7"
services:

  nginx:
    image: nginx
    container_name: nginx
    ports:
      - "80:80"
~~~
___$ docker-compose up -d___
~~~
Creating compose_nginx_1 ... done
~~~
___$ docker-compose ps___
~~~
     Name               Command          State         Ports
-------------------------------------------------------------------
nginx   nginx -g daemon off;   Up      0.0.0.0:80->80/tcp
~~~
___$ vi docker-compose-yml___
~~~
version: "3.7"
services:

  nginx:
    image: nginx
    container_name: nginx
    ports:
      - "80:80"
  mysql:
    image: mysql
    container_name: mysql
~~~
___$ docker-compose up -d___
~~~
Pulling mysql (mysql:)...
latest: Pulling from library/mysql
c499e6d256d6: Already exists
22c4cdf4ea75: Pull complete
6ff5091a5a30: Pull complete
2fd3d1af9403: Pull complete
0d9d26127d1d: Pull complete
54a67d4e7579: Pull complete
fe989230d866: Pull complete
3a808704d40c: Pull complete
826517d07519: Pull complete
69cd125db928: Pull complete
b5c43b8c2879: Pull complete
1811572b5ea5: Pull complete
Digest: sha256:b69d0b62d02ee1eba8c7aeb32eba1bb678b6cfa4ccfb211a5d7931c7755dc4a8
Status: Downloaded newer image for mysql:latest
Recreating compose_nginx_1 ... done
Creating mysql             ... done
~~~
___$ docker-compose ps___
~~~
Name              Command             State          Ports
-----------------------------------------------------------------
mysql   docker-entrypoint.sh mysqld   Exit 1
nginx   nginx -g daemon off;          Up       0.0.0.0:80->80/tcp
~~~
___$ docker-compose logs mysql___
~~~
Attaching to mysql
mysql    | 2020-04-04 01:44:13+00:00 [Note] [Entrypoint]: Entrypoint script for MySQL Server 8.0.19-1debian10 started.
mysql    | 2020-04-04 01:44:13+00:00 [Note] [Entrypoint]: Switching to dedicated user 'mysql'
mysql    | 2020-04-04 01:44:13+00:00 [Note] [Entrypoint]: Entrypoint script for MySQL Server 8.0.19-1debian10 started.
mysql    | 2020-04-04 01:44:14+00:00 [ERROR] [Entrypoint]: Database is uninitialized and password option is not specified
mysql    | 	You need to specify one of MYSQL_ROOT_PASSWORD, MYSQL_ALLOW_EMPTY_PASSWORD and MYSQL_RANDOM_ROOT_PASSWORD
~~~
___$ docker-compose down___
~~~

~~~
___$ vi docker-compose-yml___
~~~
version: "3.7"
services:

  nginx:
    image: nginx
    container_name: nginx
    ports:
      - "80:80"
  mysql:
    image: mysql
    container-name: mysql
    ports:
      - "3306:3306"
    environment:
      MYSQL_ROOT_PASSWORD: root
~~~
___$ docker-compose ps___
~~~
Name              Command             State                 Ports
-------------------------------------------------------------------------------
mysql   docker-entrypoint.sh mysqld   Up      0.0.0.0:3306->3306/tcp, 33060/tcp
nginx   nginx -g daemon off;          Up      0.0.0.0:80->80/tcp
~~~
### Variables de entorno
___$ vi .env___
~~~
MYSQL_ROOT_PASSWORD=root
~~~
___$ docker-compose down___
~~~
Stopping nginx ... done
Stopping mysql ... done
Removing nginx ... done
Removing mysql ... done
Removing network compose_default
~~~
___$ vi docker-compose-yml___
~~~
version: "3.7"

services:

   nginx:
      image: nginx
      container-name: nginx
      ports:
          - "80:80"
   mysql:
      image: mysql
      container-name: mysql
      ports:
          - "3306:3306"
      environment:
         MYSQL_ROOT_PASSWORD: ${MYSQL_ROOT_PASSWORD}
~~~
___$ docker-compose up -d___
~~~
mysql is up-to-date
nginx is up-to-date
~~~
___$ docker-compose ps___
~~~
Name              Command             State                 Ports
-------------------------------------------------------------------------------
mysql   docker-entrypoint.sh mysqld   Up      0.0.0.0:3306->3306/tcp, 33060/tcp
nginx   nginx -g daemon off;          Up      0.0.0.0:80->80/tcp
~~~
### Redes

___$ docker network ls___
~~~
NETWORK ID          NAME                DRIVER              SCOPE
afc4541d3264        bridge              bridge              local
487ee790d7f2        compose_default     bridge              local
448d20b4f2a6        host                host                local
9912550a69f9        none                null                local
~~~
___$ docker network inspect compose_default___
~~~
[
    {
        "Name": "compose_default",
        "Id": "487ee790d7f2088040487a998ace0ee96d47861dcbc0c3cdedb2801b84007824",
        "Created": "2020-04-03T20:16:10.680597934-06:00",
        "Scope": "local",
        "Driver": "bridge",
        "EnableIPv6": false,
        "IPAM": {
            "Driver": "default",
            "Options": null,
            "Config": [
                {
                    "Subnet": "172.20.0.0/16",
                    "Gateway": "172.20.0.1"
                }
            ]
        },
        "Internal": false,
        "Attachable": true,
        "Ingress": false,
        "ConfigFrom": {
            "Network": ""
        },
        "ConfigOnly": false,
        "Containers": {
            "4611f582ad3440b0ed4e7de47bdcbca59758fb06e1898dc066d7fcd8f5909551": {
                "Name": "nginx",
                "EndpointID": "9ddd6d3a4ebd49cada4b6b6c3eb2d27f03699996672ba47f55581e73b410b607",
                "MacAddress": "02:42:ac:14:00:02",
                "IPv4Address": "172.20.0.2/16",
                "IPv6Address": ""
            },
            "8c906fa24a51c6a381a4d941a00704fe005776da5a49b1b3c4c54ff5422e1e6b": {
                "Name": "mysql",
                "EndpointID": "9c0cffe0e5fcbeb633fb9d2b8f3523e0232ce039a3e73fb00a3020383a083ee1",
                "MacAddress": "02:42:ac:14:00:03",
                "IPv4Address": "172.20.0.3/16",
                "IPv6Address": ""
            }
        },
        "Options": {},
        "Labels": {
            "com.docker.compose.network": "default",
            "com.docker.compose.project": "compose",
            "com.docker.compose.version": "1.25.4"
        }
    }
]
~~~
___$ vi docker-compose.yml___
~~~
version: "3.7"
services:

  nginx:
    image: nginx
    container_name: nginx
    ports:
      - "80:80"
  mysql:
    image: mysql
    container_name: mysql
      ports:
        - "3306:3306"
      networks:
        - net-demo
      environment:
        MYSQL_ROOT_PASSWORD: ${MYSQL_ROOT_PASSWORD}

networks:
  net-demo:
    driver: bridge
~~~
___$ docker-compose up -d___
~~~
Creating network "compose_net-demo" with driver "bridge"
nginx is up-to-date
Recreating mysql ... done
~~~
___$ docker network inspect compose-net-demo___
~~~
[
    {
        "Name": "compose_net-demo",
        "Id": "4aa2833e2288c16b23dc76c385811b0f7116b8b22770e197ab3bd679a8dc54a6",
        "Created": "2020-04-03T21:24:13.260259657-06:00",
        "Scope": "local",
        "Driver": "bridge",
        "EnableIPv6": false,
        "IPAM": {
            "Driver": "default",
            "Options": null,
            "Config": [
                {
                    "Subnet": "172.21.0.0/16",
                    "Gateway": "172.21.0.1"
                }
            ]
        },
        "Internal": false,
        "Attachable": true,
        "Ingress": false,
        "ConfigFrom": {
            "Network": ""
        },
        "ConfigOnly": false,
        "Containers": {
            "fbf44a3c1bbcd955368338c17c821803d4a449e1af31a3e2e19ca9db33e2429d": {
                "Name": "mysql",
                "EndpointID": "73fcfee666b8aad6d247da11d973b313af502216990f5efc9ca9ccda39bc0996",
                "MacAddress": "02:42:ac:15:00:02",
                "IPv4Address": "172.21.0.2/16",
                "IPv6Address": ""
            }
        },
        "Options": {},
        "Labels": {
            "com.docker.compose.network": "net-demo",
            "com.docker.compose.project": "compose",
            "com.docker.compose.version": "1.25.4"
        }
    }
]
~~~
___$ docker network inspect compose_default___
~~~
[
    {
        "Name": "compose_default",
        "Id": "487ee790d7f2088040487a998ace0ee96d47861dcbc0c3cdedb2801b84007824",
        "Created": "2020-04-03T20:16:10.680597934-06:00",
        "Scope": "local",
        "Driver": "bridge",
        "EnableIPv6": false,
        "IPAM": {
            "Driver": "default",
            "Options": null,
            "Config": [
                {
                    "Subnet": "172.20.0.0/16",
                    "Gateway": "172.20.0.1"
                }
            ]
        },
        "Internal": false,
        "Attachable": true,
        "Ingress": false,
        "ConfigFrom": {
            "Network": ""
        },
        "ConfigOnly": false,
        "Containers": {
            "4611f582ad3440b0ed4e7de47bdcbca59758fb06e1898dc066d7fcd8f5909551": {
                "Name": "nginx",
                "EndpointID": "9ddd6d3a4ebd49cada4b6b6c3eb2d27f03699996672ba47f55581e73b410b607",
                "MacAddress": "02:42:ac:14:00:02",
                "IPv4Address": "172.20.0.2/16",
                "IPv6Address": ""
            }
        },
        "Options": {},
        "Labels": {
            "com.docker.compose.network": "default",
            "com.docker.compose.project": "compose",
            "com.docker.compose.version": "1.25.4"
        }
    }
]
~~~
___$ vi docker-compose-yml___
~~~
version: "3.7"
services:

  nginx:
    image: nginx
    container_name: nginx
    ports:
      - "80:80"
  mysql:
    image: mysql
    container_name: mysql
    ports:
      - "3306:3306"
    networks:
      - b0
    environment:
      MYSQL_ROOT_PASSWORD: ${MYSQL_ROOT_PASSWORD}

networks:
  b0:
    external: true
~~~
external: true se refiere a una red que ya existe, en caso contrario es necesario crearla primero y despues definirla, de todos modos enviará un error.

___$ docker-compose up -d___
~~~
Recreating mysql ...
Recreating mysql ... done
~~~
___$ docker network inspect b0___
~~~
[
    {
        "Name": "b0",
        "Id": "981479e8ce3e0ec1c54c9ac793cc3166dd62a0bc904201ef2c2ed63e28d1dc63",
        "Created": "2020-04-03T22:45:34.491028486-06:00",
        "Scope": "local",
        "Driver": "bridge",
        "EnableIPv6": false,
        "IPAM": {
            "Driver": "default",
            "Options": {},
            "Config": [
                {
                    "Subnet": "172.24.0.0/16",
                    "Gateway": "172.24.0.1"
                }
            ]
        },
        "Internal": false,
        "Attachable": false,
        "Ingress": false,
        "ConfigFrom": {
            "Network": ""
        },
        "ConfigOnly": false,
        "Containers": {
            "24b4f9ae78b1144cfbabfcca85fcb037a705ad4cc516484e1be24584cb092be2": {
                "Name": "mysql",
                "EndpointID": "73ec5b774ae8901a74c00e2a8885a0a6777bc982a5aaa01408db50e3437abec1",
                "MacAddress": "02:42:ac:18:00:02",
                "IPv4Address": "172.24.0.2/16",
                "IPv6Address": ""
            }
        },
        "Options": {},
        "Labels": {}
    }
]
~~~
### Volúmenes
___$ vi docker-compose-yml___
~~~
version: "3.7"
services:

  nginx:
    image: nginx
    container_name: nginx
    ports:
      - "80:80"
  mysql:
    image: mysql
    container_name: mysql
    ports:
      - "3306:3306"
    networks:
      - b0
    environment:
      MYSQL_ROOT_PASSWORD: ${MYSQL_ROOT_PASSWORD}
    volumes:
      - mysql:/var/lib/mysql

networks:
  b0:
    external: true

volumes:
  mysql:
~~~

___$ docker-compose up -d___
~~~
nginx is up-to-date
Recreating mysql ...
WARNING: Service "mysql" is using volume "/var/lib/mysql" from the previous container. Host mapping "compose_mysql" has no effect. RemRecreating mysql ... done
~~~
___$ docker volumes ls___
~~~
DRIVER              VOLUME NAME
local               4de749f2e4d1df299f81c6306176fa5c0e959bf7a8323620178fae3dc1c4771a
local               5dc56bc415d0789a7f56e93b3a3e92c6b5e7dbe807df0b5b26fc790d60b29e3b
local               2832fe62c5e26ccfab9592a51294b3eecf27b751af6e2b092d89787b63319107
local               9623d9a4024dc9c875e333dd1d67d0511f375baeeeebd7eeea4a20da9991ff73
local               compose_mysql
local               portainer_data
~~~
___$ vi index.html___

Se le agrega contenido al archivo index.html y se deja dentro del mismo directorio de docker-compose.

___$ vi index.html___
~~~
<html>
<h1> Hola Mundo desde docker-compose</h1>
</html>
~~~
___$ vi docker-compose-yml___
~~~
version: "3.7"
services:

  nginx:
    image: nginx
    container_name: nginx
    ports:
      - "80:80"
    volumes:
      - ./index.html:/usr/share/nginx/html/index.html
  mysql:
    image: mysql
    container_name: mysql
    ports:
      - "3306:3306"
    networks:
      - b0
    environment:
      MYSQL_ROOT_PASSWORD: ${MYSQL_ROOT_PASSWORD}
    volumes:
      - mysql:/var/lib/mysql

networks:
  b0:
    external: true

volumes:
  mysql:
~~~

___$ docker-compose up -d___
~~~
Recreating nginx ...
Recreating nginx ... done
~~~
Desde el browser abrir localhost

## 8. Orquestadores
***

### Orquestadores teoría

### Docker swarm

___$ docker-machine create -d virtualbox manager1___

___$ docker-machine create -d virtualbox worker1___

___$ docker-machine create -d virtualbox worker2___

___$ docker-machine ls___
~~~
NAME       ACTIVE   DRIVER       STATE     URL                         SWARM   DOCKER     ERRORS
manager1   -        virtualbox   Running   tcp://192.168.99.101:2376           v19.03.5
worker1    -        virtualbox   Running   tcp://192.168.99.102:2376           v19.03.5
worker2    -        virtualbox   Running   tcp://192.168.99.103:2376           v19.03.5
~~~
___$ docker-machine ssh manager1___
~~~
docker-machine ssh manager1
   ( '>')
  /) TC (\   Core is distributed with ABSOLUTELY NO WARRANTY.
 (/-_--_-\)           www.tinycorelinux.net

docker@manager1:~$ docker swam
docker: 'swam' is not a docker command.
See 'docker --help'
docker@manager1:~$ docker swarm

Usage:	docker swarm COMMAND

Manage Swarm

Commands:
  ca          Display and rotate the root CA
  init        Initialize a swarm
  join        Join a swarm as a node and/or manager
  join-token  Manage join tokens
  leave       Leave the swarm
  unlock      Unlock swarm
  unlock-key  Manage the unlock key
  update      Update the swarm

Run 'docker swarm COMMAND --help' for more information on a command.

docker@manager1:~$ docker swarm init --help

Usage:	docker swarm init [OPTIONS]

Initialize a swarm

Options:
      --advertise-addr string                  Advertised address (format: <ip|interface>[:port])
      --autolock                               Enable manager autolocking (requiring an unlock key to start a stopped manager)
      --availability string                    Availability of the node ("active"|"pause"|"drain") (default "active")
      --cert-expiry duration                   Validity period for node certificates (ns|us|ms|s|m|h) (default 2160h0m0s)
      --data-path-addr string                  Address or interface to use for data path traffic (format: <ip|interface>)
      --data-path-port uint32                  Port number to use for data path traffic (1024 - 49151). If no value is set or
                                               is set to 0, the default port (4789) is used.
      --default-addr-pool ipNetSlice           default address pool in CIDR format (default [])
      --default-addr-pool-mask-length uint32   default address pool subnet mask length (default 24)
      --dispatcher-heartbeat duration          Dispatcher heartbeat period (ns|us|ms|s|m|h) (default 5s)
      --external-ca external-ca                Specifications of one or more certificate signing endpoints
      --force-new-cluster                      Force create a new cluster from current state
      --listen-addr node-addr                  Listen address (format: <ip|interface>[:port]) (default 0.0.0.0:2377)
      --max-snapshots uint                     Number of additional Raft snapshots to retain
      --snapshot-interval uint                 Number of log entries between Raft snapshots (default 10000)
      --task-history-limit int                 Task history retention limit (default 5)

docker@manager1:~$ docker swarm init --advertise-addr 192.168.99.101
Swarm initialized: current node (wcq1hg9ltlffuxfxsg5b10eo7) is now a manager.

To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-0cyxwoblb62r2wec9tcf8j7lbu51o4d6mjpkk4shwbbhzilg1n-8f7p16hyd97lxavf4kwztstij 192.168.99.101:2377

To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.

docker@manager1:~$ exit
logout
~~~

Se inicializa docker swarm en el manager1 y después de integran los workers.

___$docker-machine ssh worker1___
~~~
   ( '>')
  /) TC (\   Core is distributed with ABSOLUTELY NO WARRANTY.
 (/-_--_-\)           www.tinycorelinux.net

docker@worker1:~$ docker swarm join --token SWMTKN-1-0cyxwoblb62r2wec9tcf8j7lbu51o4d6mjpkk4shwbbhzilg1n-8f7p16hyd97lxavf4kwztstij 192.168.99.101:2377
This node joined a swarm as a worker.

docker@worker1:~$ exit
logout
~~~

___$docker-machine ssh worker2___
~~~

   ( '>')
  /) TC (\   Core is distributed with ABSOLUTELY NO WARRANTY.
 (/-_--_-\)           www.tinycorelinux.net

docker@worker2:~$ docker swarm join --token SWMTKN-1-0cyxwoblb62r2wec9tcf8j7lbu51o4d6mjpkk4shwbbhzilg1n-8f7p16hyd97lxavf4kwztstij 192.168.99.101:2377
This node joined a swarm as a worker.
docker@worker2:~$ exit
logout
~~~
___$ docker-machine ssh manager1___
~~~
   ( '>')
  /) TC (\   Core is distributed with ABSOLUTELY NO WARRANTY.
 (/-_--_-\)           www.tinycorelinux.net

docker@manager1:~$ docker node ls
ID                            HOSTNAME            STATUS              AVAILABILITY        MANAGER STATUS      ENGINE VERSION
wcq1hg9ltlffuxfxsg5b10eo7 *   manager1            Ready               Active              Leader              19.03.5
p9b7n6p884r92ygrzfny9adq6     worker1             Ready               Active                                  19.03.5
qp5u5l26e0u5xdavsemeg5oui     worker2             Ready               Active                                  19.03.5
~~~

### Docker swarm deploy servicio
~~~
docker@manager1:~$ docker service create --replicas 1 --name redis redis:4
iki9zbrc96fdsb0brb3ks02c5
overall progress: 1 out of 1 tasks
1/1: running   [==================================================>]
verify: Service converged

docker@manager1:~$ docker service ls
ID                  NAME                MODE                REPLICAS            IMAGE               PORTS
iki9zbrc96fd        redis               replicated          1/1                 redis:4

docker@manager1:~$ docker service inspect --pretty redis

ID:		iki9zbrc96fdsb0brb3ks02c5
Name:		redis
Service Mode:	Replicated
 Replicas:	1
Placement:
UpdateConfig:
 Parallelism:	1
 On failure:	pause
 Monitoring Period: 5s
 Max failure ratio: 0
 Update order:      stop-first
RollbackConfig:
 Parallelism:	1
 On failure:	pause
 Monitoring Period: 5s
 Max failure ratio: 0
 Rollback order:    stop-first
ContainerSpec:
 Image:		redis:4@sha256:3f21afab41dabaa4f0cea09e6feb74fe0626dd148c2bcffe40f6f54c4778cac3
 Init:		false
Resources:
Endpoint Mode:	vip
~~~

### Docker swarm actualizar puertos
~~~
docker@manager1:~$ docker service create --replicas 1 --name nginx nginx
5539fmi25ko1xdcbw5n736rrj
overall progress: 1 out of 1 tasks
1/1: running   [==================================================>]
verify: Service converged

docker@manager1:~$ docker service ps nginx
ID                  NAME                IMAGE               NODE                DESIRED STATE       CURRENT STATE            ERROR               PORTS
7ivsfjruj8ce        nginx.1             nginx:latest        worker2             Running             Running 11 minutes ago
~~~

Desde otra terminal vemos si el servicio esta disponible desde la ip del worker2

___$ docker-machine ls___
~~~
NAME       ACTIVE   DRIVER       STATE     URL                         SWARM   DOCKER     ERRORS
manager1   -        virtualbox   Running   tcp://192.168.99.101:2376           v19.03.5
worker1    -        virtualbox   Running   tcp://192.168.99.102:2376           v19.03.5
worker2    -        virtualbox   Running   tcp://192.168.99.103:2376           v19.03.5
~~~

Por medio del browser damos la ip del worker2 (192.168.99.103) y nos dirá que el servicio no esta disponible.

Nuevamente dentro de manager1 actualizamos el servicio y se publica.

~~~
docker@manager1:~$ docker service update --publish-add 80:80 nginx
nginx
overall progress: 1 out of 1 tasks
1/1: running   [==================================================>]
verify: Service converged
docker@manager1:~$
docker@manager1:~$ docker service ps nginx
ID                  NAME                IMAGE               NODE                DESIRED STATE       CURRENT STATE                 ERROR               PORTS
i5blkm4o9gfs        nginx.1             nginx:latest        worker1             Running             Running 41 seconds ago
7ivsfjruj8ce         \_ nginx.1         nginx:latest        worker2             Shutdown            Shutdown about a minute ago
~~~

En la otra terminal revisamos los servicios de worker2.

~~~
docker@worker2:~$ docker ps -a
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS               NAMES
7b01c0f25cf7        nginx:latest        "nginx -g 'daemon of…"   16 minutes ago      Up 16 minutes       80/tcp              nginx.1.7ivsfjruj8cedjsp89q45pk1i
~~~

Ahora entramos al navegador nuevamente con la ip del worker2 y el servicio de nginx debe responder.

Desde manager1 actualizamos el servicio y subimos el número de replicas a 3.

~~~
docker@manager1:~$ docker service update --replicas 3 nginx
nginx
overall progress: 3 out of 3 tasks
1/3: running   [==================================================>]
2/3: running   [==================================================>]
3/3: running   [==================================================>]
verify: Service converged
~~~

Ahora los servicios deben verse distribuidos entre los nodos.

~~~
docker@manager1:~$ docker service ps nginx

ID                  NAME                IMAGE               NODE                DESIRED STATE       CURRENT STATE             ERROR               PORTS
i5blkm4o9gfs        nginx.1             nginx:latest        worker1             Running             Running 18 minutes ago
7ivsfjruj8ce         \_ nginx.1         nginx:latest        worker2             Shutdown            Shutdown 19 minutes ago
rpzyzn8xqvcs        nginx.2             nginx:latest        worker2             Running             Running 3 minutes ago
jqvjwl00gc9g        nginx.3             nginx:latest        manager1            Running             Running 2 minutes ago
~~~

Si entramos desde el navegador a las ips de los 3 nodos el servicio nginx debe estar arriba.
